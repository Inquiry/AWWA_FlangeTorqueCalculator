﻿interface IStaticValues {
    //public properties
    nominalPipeSize: number;
    numberOfBolts: number;
    boltSizeDiameter: string;
    boltSize: number;
    threadsPerInch: number;
    heavyHexNutSize: string;
    minYieldStress: number;
    flangeID: number;
    flangeOD: number;
    boltCircle: number;
    seatingStress: number;
    crushingStress: number;
}

interface ICalculatedValues extends IStaticValues {
    nominalBoltArea: number;
    boltCircleID: number;
    ringArea: number;
    fullArea: number;
    ringMin: number;
    ringMinPercent: number;
    ringMax: number;
    ringMaxPercent: number;
    fullMin: number;
    fullMinPercent: number;
    fullMax: number;
    fullMaxPercent: number;
    boltTension_50percent: number;
    gasketSeatingFaceArea: number;
    boltCrossSection: number;

    getGasketPercent: Function;
}

interface ICalculatorInputs {
    //flange inputs
    flangeClass: FlangeClass;
    nominalFlangeDiameter: number;
    finishedFlangeSeatingFaceId?: number;

    //gasket inputs
    gasketStyle: GasketStyle;
    gasketMaterial: MaterialName;

    //calculation constant, K
    selectedNutFactor: number;
}