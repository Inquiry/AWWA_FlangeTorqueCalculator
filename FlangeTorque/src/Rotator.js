var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
function fillRotator() {
    return __awaiter(this, void 0, void 0, function () {
        var linkedProductDiv, i, product;
        return __generator(this, function (_a) {
            linkedProductDiv = $(".linked-products")[0];
            //for (let i = 0; i < activeProducts.length; i++) {
            //    linkedProductDiv.innerHTML += `<div class="linked-product">` +
            //        `<a href="${activeProducts[i].url}">` +
            //        `<img src="images/relatedProducts/${activeProducts[i].image}" alt="${activeProducts[i].title}">` +
            //        `<p style="color:inherit;">${activeProducts[i].title}</p>` +
            //        `</a>` +
            //        `<p>LIST PRICE: <span>${activeProducts[i].listPrice}</span></p>` +
            //        `<p>MEMBER PRICE: <span class="member-price">${activeProducts[i].memberPrice}</span></p>` +
            //        `</div>`;
            //}
            for (i = 0; i < activeProductIds.length; i++) {
                product = new Product(activeProductIds[i]);
                if (!product.title)
                    continue; //no need to populate something if no info is grabbed
                linkedProductDiv.innerHTML += "<div class=\"linked-product\">" +
                    ("<a href=\"" + product.url + "\">") +
                    ("<img src=\"images/relatedProducts/" + product.image + "\" alt=\"" + product.title + "\">") +
                    ("<p style=\"color:inherit;\">" + product.title + "</p>") +
                    "</a>" +
                    ("<p>LIST PRICE: <span>" + product.listPrice + "</span></p>") +
                    ("<p>MEMBER PRICE: <span class=\"member-price\">" + product.memberPrice + "</span></p>") +
                    "</div>";
            }
            return [2 /*return*/];
        });
    });
}
/**
 * Scrapes an url for product metadata in order to populate itself
 */
var Product = (function () {
    function Product(productId) {
        this.url = domain + storeProductBase + productId;
        //using jQuery because that was the first thing I could find
        $.get(this.url, function (data) {
            this.title = $(data).find("meta[property=og:title]").attr("content");
            this.image = $(data).find("meta[property=og:image]").attr("content");
            this.listPrice = $(data).find("meta[property=og:price]").attr("content");
            this.memberPrice = $(data).find("span[id*=ProductINVBuyBoxControl_PriceDisplayControl1_MemberPrice_PriceValueLabel").val();
        });
    }
    Object.defineProperty(Product.prototype, "listPrice", {
        get: function () {
            return new Intl.NumberFormat("en", { style: "currency", currency: "USD" }).format(this._listPrice);
        },
        set: function (amount) {
            amount = amount.replace(/\D/g, "");
            this._listPrice = parseFloat(amount) ? parseFloat(amount) : 0.0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "memberPrice", {
        get: function () {
            return new Intl.NumberFormat("en", { style: "currency", currency: "USD" }).format(this._memberPrice);
        },
        set: function (amount) {
            amount = amount.replace(/^\D/, "");
            this._memberPrice = parseFloat(amount) ? parseFloat(amount) : 0.0;
        },
        enumerable: true,
        configurable: true
    });
    return Product;
}());
//# sourceMappingURL=Rotator.js.map