﻿class Outputs {
    //looked up or calculated values
    public input: ICalculatorInputs;
    public material: Material;
    public flange: Flange;
    public calculations: ICalculatedValues;
    public boltLoad: BoltLoad;

    //Torque
    public loadAtGasketSeating: number;
    public loadAtGasketCrushing: number;
    public percentAtSeating: number;
    public percentAtCrushing: number;
    public torqueAtGasketSeating: number;
    public torqueAtGasketCrushing: number;

    //Torque Ranges
    public minRecommendedBoltTorque: number;
    public maxRecommendedBoltTorque: number;
    public boltLoadPsiMin: number;
    public boltLoadPsiMax: number;
    public boltLoadPercentMin: number;
    public boltLoadPercentMax: number;

    //output storeage
    public outputSections: [OutputSection];

    constructor(calcInput: ICalculatorInputs) {
        this.calculateOutputs(calcInput);
    }


    /**
     * Calculates all the output values before being assigned to the sections
     * @param calcInput
     * @param loadOutputs
     */
    private async calculateOutputs(calcInput: ICalculatorInputs) {
        this.input = calcInput;
        this.material = new Material(calcInput.gasketMaterial);
        this.flange = new Flange(calcInput);
        this.calculations = new CalculatedValues(this.flange, this.material, calcInput);
        this.boltLoad = new BoltLoad(this.calculations, calcInput.selectedNutFactor);

        if (this.material && this.flange && this.calculations && this.boltLoad) {
            this.loadAtGasketSeating = this.calculatePsi(this.calculations.seatingStress, this.calculations.gasketSeatingFaceArea,
                this.calculations.numberOfBolts, this.calculations.boltCrossSection);
            this.loadAtGasketCrushing = this.calculatePsi(this.calculations.crushingStress, this.calculations.gasketSeatingFaceArea,
                this.calculations.numberOfBolts, this.calculations.boltCrossSection);
        } else {
            throw "still not calculating";
        }
        await this.secondCalc();
    }

    private async secondCalc() {
        this.torqueAtGasketSeating = this.calculateTorque(this.input.selectedNutFactor, this.calculations.boltSize, this.loadAtGasketSeating);
        this.torqueAtGasketCrushing = this.calculateTorque(this.input.selectedNutFactor, this.calculations.boltSize, this.loadAtGasketCrushing);

        this.percentAtSeating = this.loadAtGasketSeating / this.calculations.minYieldStress;
        this.percentAtCrushing = this.loadAtGasketCrushing / this.calculations.minYieldStress;

        let ranges = await this.getRecommendedTorque();
        this.maxRecommendedBoltTorque = ranges.max;
        this.minRecommendedBoltTorque = ranges.min;
    }

    async calcPsiMinMax() {
        this.boltLoadPsiMin = this.getBoltLoadPsiAtTorque(this.minRecommendedBoltTorque, this.input.selectedNutFactor, this.calculations.boltSize);
        this.boltLoadPsiMax = this.getBoltLoadPsiAtTorque(this.maxRecommendedBoltTorque, this.input.selectedNutFactor, this.calculations.boltSize);
    }

    async calcLoadPercentMinMax() {
        this.boltLoadPercentMin = this.boltLoadPsiMin / this.calculations.minYieldStress;
        this.boltLoadPercentMax = this.boltLoadPsiMax / this.calculations.minYieldStress;
    }

    private calculatePsi(stress: number, gasketSeatingFaceArea: number, numberOfBolts: number, boltCrossSection: number): number {
        let psi = gasketSeatingFaceArea * stress / numberOfBolts / boltCrossSection;
        return psi;
    }

    private calculateTorque(nutFactor: number, boltDiameter: number, boltLoading: number): number {

        let torque = nutFactor * boltDiameter * boltLoading / 12.0;
        return torque;
    }


    private async getRecommendedTorque(): Promise<{ max: number, min: number }> {
        let range = { max: 0, min: 0 };

        if (!this.boltLoad.highTorque)
            await this.boltLoad;
        range.max = Math.max(this.torqueAtGasketSeating * 1.15, Math.min(this.torqueAtGasketCrushing * 0.9, this.input.selectedNutFactor * this.calculations.boltSize * 1.1 * this.boltLoad.targetPsi / 12.0));
        range.max = Math.round(range.max / 5.0) * 5.0;//range.max % 5.0 >= 2.5 ? Math.round(range.max / 5.0) * 5.0 + 5.0 : Math.round(range.max / 5.0) * 5.0;

        let minTorque = this.input.selectedNutFactor * this.calculations.boltSize * 0.9 * this.boltLoad.targetPsi / 12.0;
        let maximumMinTorque = this.torqueAtGasketSeating * 1.1;
        if (Math.max(maximumMinTorque, minTorque) > 0.8 * range.max) {
            range.min = 0.8 * range.max;
        } else {
            range.min = Math.max(maximumMinTorque, minTorque);
            range.min = Math.round(range.min / 5.0) * 5.0;//range.min % 5.0 >= 2.5 ? Math.round(range.min / 5.0) * 5.0 + 5.0 : Math.round(range.min / 5.0) * 5.0;
        }

        return range;
    }

    private getBoltLoadPsiAtTorque(recommendedTorque: number, selectedNutFactor: number, boltSize: number): number {
        return recommendedTorque * 12.0 / selectedNutFactor / boltSize;
    }
}

async function formatOutputs(calcOutputs: Outputs): Promise<[OutputSection]> {
        
        let flangeDetails = new OutputSection;
        flangeDetails.sectionId = "fd";
        flangeDetails.values = [
            calcOutputs.calculations.numberOfBolts.toString(),
            calcOutputs.calculations.boltSize.toFixed(3) + " in.", //truncate to 3 decimal places
            new Intl.NumberFormat("en",{ style: "decimal"}).format(calcOutputs.calculations.minYieldStress) + " psi",
            calcOutputs.calculations.flangeID.toFixed(2) + " in.", //truncate to 2 decimal places
            calcOutputs.calculations.flangeOD + " in.",
            calcOutputs.calculations.boltCircle.toFixed(2) + " in.", //truncate to 2 decimal places
            calcOutputs.calculations.gasketSeatingFaceArea.toFixed(1) + " in.<sup>2</sup>", //truncate to 1 decimal place
            calcOutputs.calculations.boltCrossSection.toFixed(3) + " in.<sup>2</sup>"
        ];

        let gasketMaterial = new OutputSection;
        gasketMaterial.sectionId = "gm";
        gasketMaterial.values = [
            new Intl.NumberFormat("en", { style: "decimal" }).format(calcOutputs.material.SeatingStress) + " psi",
            new Intl.NumberFormat("en", { style: "decimal" }).format(calcOutputs.material.CrushingStress) + " psi"
        ];

        let threadLubricant = new OutputSection;
        threadLubricant.sectionId = "tl";
        threadLubricant.values = [
            calcOutputs.input.selectedNutFactor.toString()
        ];

        let boltYieldLoad = new OutputSection;
        boltYieldLoad.sectionId = "byl";
        boltYieldLoad.values = [
            new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.loadAtGasketSeating) + " psi",
            new Intl.NumberFormat("en", { style: "percent" }).format(calcOutputs.percentAtSeating),
            new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.loadAtGasketCrushing) + " psi",
            new Intl.NumberFormat("en", { style: "percent" }).format(calcOutputs.percentAtCrushing),
            new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0  }).format(calcOutputs.torqueAtGasketSeating) + " ft-lbs",
            new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0  }).format(calcOutputs.torqueAtGasketCrushing) + " ft-lbs",
        ];

        return [flangeDetails, gasketMaterial, threadLubricant, boltYieldLoad] as [OutputSection];
    }


class OutputSection {
    sectionId: string;
    values: [string];
}