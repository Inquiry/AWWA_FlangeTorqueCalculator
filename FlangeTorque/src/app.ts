﻿function loadClassDropDown() {
    let flangeClassDropDown = document.getElementById("flangeClassDropDown") as HTMLSelectElement;

    for (let i = 0; i < flangeClasses.length; i++) {
        let option = document.createElement("option");
        option.text = flangeClasses[i].class;
        option.value = flangeClasses[i].id.toString();
        flangeClassDropDown.add(option);
    }

    flangeClassDropDown.selectedIndex = -1;
}

/**
 * Holds the value of the currently selected class between changes
 */
var currentlySelectedPipeClass = -1;

function loadPipeSizeDropDown() {
    let newPipeClassId = parseInt((document.getElementById("flangeClassDropDown") as HTMLSelectElement).value);
    let isDiameterListTheSame = true;

    //checks to see if a change is needed to the diameter dropdown
    if ((currentlySelectedPipeClass != FlangeClass.F && newPipeClassId === FlangeClass.F)
        || (currentlySelectedPipeClass === FlangeClass.F && newPipeClassId != FlangeClass.F)
        || newPipeClassId >= 0) {
        isDiameterListTheSame = false;
    }

    //ensures each new selection can be checked
    currentlySelectedPipeClass = newPipeClassId;

    if (isDiameterListTheSame) {
        return;
    }

    let diameterDropDown = document.getElementById("flangeDiameterDropDown") as HTMLSelectElement;
    let activeFlangeValues = [];

    //the nominal pipe diameters only change on the F class. I don't know why
    if (newPipeClassId) {
        activeFlangeValues = classF_Values;
    } else {
        activeFlangeValues = classBDE_Values;
    }

    if (diameterDropDown.length > 0) { while (diameterDropDown.length > 0) { diameterDropDown.remove(0); } }

    for (let i = 0; i < activeFlangeValues.length; i++) {
        let option = document.createElement("option");
        option.text = activeFlangeValues[i].NominalPipeSize.toString();
        option.value = activeFlangeValues[i].NominalPipeSize.toString();
        diameterDropDown.add(option);
    }

    diameterDropDown.selectedIndex = -1;
}

function loadGasketDetails() {
    let gasketStyleDropDown = document.getElementById("gasketStyleDropDown") as HTMLSelectElement;
    let gasketMaterialDropDown = document.getElementById("gasketMaterialDropDown") as HTMLSelectElement;

    for (let i = 0; i < gasketDetails.style.length; i++) {
        let option = document.createElement("option");
        option.text = gasketDetails.style[i].type;
        option.value = gasketDetails.style[i].id.toString();
        gasketStyleDropDown.add(option);
    }
    for (let i = 0; i < gasketDetails.material.length; i++) {
        let option = document.createElement("option");
        option.text = gasketDetails.material[i].name;
        option.value = gasketDetails.material[i].id.toString();
        gasketMaterialDropDown.add(option);
    }

    gasketStyleDropDown.selectedIndex = -1;
    gasketMaterialDropDown.selectedIndex = -1;
}

/**
 * Required in order to wait for the class dropdown to load first
 */
async function loadFlangeDetails() {
    await loadClassDropDown();
    loadPipeSizeDropDown();
}

function loadInputs() {
    let inputs: ICalculatorInputs;
    inputs = { flangeClass: 0, nominalFlangeDiameter: 0, finishedFlangeSeatingFaceId: 0, gasketStyle: 0, gasketMaterial: 0, selectedNutFactor: 0 }

    //flange details
    inputs.flangeClass = parseInt((document.getElementById("flangeClassDropDown") as HTMLSelectElement).value);
    inputs.nominalFlangeDiameter = parseInt((document.getElementById("flangeDiameterDropDown") as HTMLSelectElement).value);
    let faceId = (document.getElementById("flangeSeatingFace") as HTMLInputElement).value.trim();
    inputs.finishedFlangeSeatingFaceId = (!faceId || faceId === "") ? undefined : parseFloat(faceId);

    //gasket details
    inputs.gasketStyle = parseInt((document.getElementById("gasketStyleDropDown") as HTMLSelectElement).value);
    inputs.gasketMaterial = parseInt((document.getElementById("gasketMaterialDropDown") as HTMLSelectElement).value);

    //selected nut factor, K
    let k = parseFloat((document.getElementById("selectedNutValue") as HTMLInputElement).value.trim());
    inputs.selectedNutFactor = (!k) ? 0.0 : k;
    return inputs;
};

async function displayFlangeTorqueCalculations(out: Outputs) {
    //recommended ranges
    if (out.torqueAtGasketSeating > out.boltLoad.highTorque) {
        $("#targetTorque_From").text("Error!");
        $("#targetTorque_To").text("Error!");
        $("#boltLoadPsi_From").text("Error!");
        $("#boltLoadPsi_To").text("Error!");
        $("#boltLoadPercent_From").text("Error!");
        $("#boltLoadPercent_To").text("Error!");

    } else {
        $("#targetTorque_From").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.minRecommendedBoltTorque) + " ft-lbs");
        $("#targetTorque_To").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.maxRecommendedBoltTorque) + " ft-lbs");


        await out.calcPsiMinMax();

        $("#boltLoadPsi_From").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.boltLoadPsiMin) + " psi");
        $("#boltLoadPsi_To").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.boltLoadPsiMax) + " psi");

        await out.calcLoadPercentMinMax();

        $("#boltLoadPercent_From").text(new Intl.NumberFormat("en", { style: "percent" }).format(out.boltLoadPercentMin));
        $("#boltLoadPercent_To").text(new Intl.NumberFormat("en", { style: "percent" }).format(out.boltLoadPercentMax));
    }

    out.outputSections = await formatOutputs(out);

    for (let i = 0; i < out.outputSections.length; i++) {
        for (let j = 0; j < out.outputSections[i].values.length; j++) {
            let field = document.getElementById(out.outputSections[i].sectionId + (j + 1)) as HTMLSpanElement;
            if (field)
                field.innerHTML = out.outputSections[i].values[j];
        }
    }

    checkForWarnings(out);
}

function changeMaterialLabel() {
    $("#material-label").text(() => {
        let material = document.getElementById("gasketMaterialDropDown") as HTMLSelectElement;

        return material.options[material.selectedIndex] ? material.options[material.selectedIndex].innerHTML : "No";

    });
}

/**
 * The main. process for calculating all the flange-torque values
 */
async function calculateFtOutputs() {

    let inputs = await loadInputs();
    let calculations = await new Outputs(inputs);

    return calculations;
}

async function init() {
    loadGasketDetails();
    await loadFlangeDetails();
    isFormFullyFilled();
    //let startingCalc = await calculateFtOutputs();
    //displayFlangeTorqueCalculations(startingCalc);
}

function checkForWarnings(calcValues: Outputs) {
    let warningLabels = document.getElementsByClassName("selection-warning");
    for (let i = 0; i < warningLabels.length; i++) {
        warningLabels[i].setAttribute("style", "display: none");
        warningLabels[i].innerHTML = "";
        switch (i) {
            case 0: {
                if (calcValues.input.finishedFlangeSeatingFaceId < calcValues.input.nominalFlangeDiameter
                    || calcValues.input.finishedFlangeSeatingFaceId > (calcValues.input.nominalFlangeDiameter + 2 + calcValues.input.nominalFlangeDiameter / 100)) {
                    warningLabels[i].setAttribute("style", "display: block");
                }
                break;
            }
            case 1: {
                if (calcValues.input.gasketStyle === GasketStyle.FullFace) {
                    if (calcValues.input.nominalFlangeDiameter > 24 || calcValues.flange.class === FlangeClass.F) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="fullFace_warning" class="warning-message">Full Face Gaskets are not allowed in AWWA C207 for Class F or nom. ' +
                            'diameters over 24”, bolt yield can be exceeded before obtaining gasket seating stress</p>';
                        //document.getElementById("fullFace_warning").style.display = "block";
                    }
                }
                if (calcValues.material.Name.toLowerCase() == "rubber") {
                    if (calcValues.flange.class === FlangeClass.E || calcValues.flange.class === FlangeClass.F) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="rubber_warning" class="warning-message">AWWA C207 does not allow Elastomeric (Rubber) Ring Gaskets for Class F, ' +
                            'or Class E; over 24 in., over 175 psi up to 12 in., over 150 psi greater than 12 in. up to 24 in. </p>';
                        //document.getElementById("rubber_warning").style.display = "block";
                    }
                }
                break;
            }
            case 2: {

                if (calcValues.torqueAtGasketSeating > calcValues.boltLoad.highTorque) {
                    warningLabels[i].setAttribute("style", "display: block");
                    warningLabels[i].innerHTML += '<p id="seatingStress_warning" class="warning-message" > Gasket seating stress develops a ' +
                        'required load in excess of the allowable bolt load, change the gasket</p>';
                }
                if (calcValues.torqueAtGasketSeating < calcValues.boltLoad.highTorque) {
                    if (calcValues.maxRecommendedBoltTorque > calcValues.boltLoad.highTorque
                        && calcValues.maxRecommendedBoltTorque <= calcValues.boltLoad.excessiveTorque) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="overstress_warning" class="warning-message">Select a value from the middle of ' +
                            'recommended target torque range to reduce possible bolt overstress.</p>';
                    }
                    if (calcValues.minRecommendedBoltTorque < calcValues.boltLoad.lowTorque) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="crushing_warning" class="warning-message">Due to potential gasket crushing, torque values are below recommended bolt loading ' +
                            'for a stable joint.<br />Selection of a different gasket material may be necessary.</p>';

                    }
                }
                break;
            }
        }
    }
}

//the logic for recalculating on input change
async function recalculateValues() {
    if (isFormFullyFilled()) {
        let newCalculation = await calculateFtOutputs();
        displayFlangeTorqueCalculations(newCalculation);
    } else {
        window.alert("One or more required fields still needs to be filled");
    }
}

function isFormFullyFilled(): boolean {
    let isFilled = true;
    //ensures all fields are populated properly before attempting a calculation
    let requiredFields = document.getElementsByClassName("required");
    for (let i = 0; i < requiredFields.length; i++) {
        if (requiredFields[i].hasAttribute("selectedIndex")) {
            if ((requiredFields[i] as HTMLSelectElement).selectedIndex === -1) {
                isFilled = false;
                requiredFields[i].className += " alert-danger";
            } else {
                requiredFields[i].className = "required calc-inputs";
            }
        } else {
            if ((requiredFields[i] as HTMLInputElement).value.trim() === "") {
                isFilled = false;
                requiredFields[i].className += " alert-danger";
            } else {
                requiredFields[i].className = "required calc-inputs";
            }
        }
    }

    return isFilled;
}

function loadLinks() {
    $(".m11-17").attr("href", domain + storeProductBase + m11_id);
    $(".c207-13").attr("href", domain + storeProductBase + c207_id);
    $(".c604-17").attr("href", domain + storeProductBase + c604_id);
    $("#contactUsButton").attr("href", domain + "/about-us/contact-us.aspx");
    $("#backToCommunityButton").attr("href", domain + "/resources-tools/water-knowledge.aspx");
    $("#instructionButton").attr("href", "/FTCResources/UserInstructions.docx");
    $("#awwaLogo").attr("href", domain);
}

//binds enter key to calculate values
$(() => {
    $(document).on("keypress", (e) => {
        if (e.which == 13) {
            recalculateValues();
        }
    });
});

/**
 * Exactly what it says on the tin: Clears the calculator's inputs for a new calculation
 */
function clearInputs() {
    let calcInputs = document.getElementsByClassName("calc-inputs");
    for (let i = 0; i < calcInputs.length; i++) {
        if (calcInputs[i].hasAttribute("selectedIndex")) {
            (calcInputs[i] as HTMLSelectElement).selectedIndex = -1;
        } else {
            (calcInputs[i] as HTMLInputElement).value = "";
        }
    }

    let diameterDropDown = document.getElementById("flangeDiameterDropDown") as HTMLSelectElement;
    if (diameterDropDown.length > 0) { while (diameterDropDown.length > 0) { diameterDropDown.remove(0); } }

    $("#targetTorque_From").text("");
    $("#targetTorque_To").text("");
    $("#boltLoadPsi_From").text("");
    $("#boltLoadPsi_To").text("");
    $("#boltLoadPercent_From").text("");
    $("#boltLoadPercent_To").text("");
    $(".calc-value-box").text("");
    $(".selection-warning").css("display", "none");
    isFormFullyFilled();
}

window.onload = () => {
    init();
    changeMaterialLabel();
    //fillRotator(); //not needed for DNN
    loadLinks();
}