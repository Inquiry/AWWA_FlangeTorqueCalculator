var Flange = (function () {
    function Flange(input) {
        this.nominalPipeSize = input.nominalFlangeDiameter;
        this.class = input.flangeClass;
        if (input.finishedFlangeSeatingFaceId)
            this.seatingFaceId = input.finishedFlangeSeatingFaceId;
    }
    return Flange;
}());
//# sourceMappingURL=Flange.js.map