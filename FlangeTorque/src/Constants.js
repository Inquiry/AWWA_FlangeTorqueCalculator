var domain = "";
var storeProductBase = "/store/productdetail.aspx?productid=";
var m11_id = 62572078;
var c207_id = 35009101;
var c604_id = 66610737;
var GasketStyle;
(function (GasketStyle) {
    GasketStyle[GasketStyle["Ring"] = 0] = "Ring";
    GasketStyle[GasketStyle["FullFace"] = 1] = "FullFace";
})(GasketStyle || (GasketStyle = {}));
var MaterialName;
(function (MaterialName) {
    MaterialName[MaterialName["Rubber"] = 0] = "Rubber";
    MaterialName[MaterialName["CompositeFiber"] = 1] = "CompositeFiber";
})(MaterialName || (MaterialName = {}));
var FlangeClass;
(function (FlangeClass) {
    FlangeClass[FlangeClass["B"] = 0] = "B";
    FlangeClass[FlangeClass["D"] = 1] = "D";
    FlangeClass[FlangeClass["E"] = 2] = "E";
    FlangeClass[FlangeClass["F"] = 3] = "F";
})(FlangeClass || (FlangeClass = {}));
//# sourceMappingURL=Constants.js.map