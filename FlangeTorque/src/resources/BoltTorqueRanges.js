var flangeClasses = [
    { "class": "B", "id": 0 },
    { "class": "D", "id": 1 },
    { "class": "E", "id": 2 },
    { "class": "F", "id": 3 }
];
var gasketDetails = {
    "style": [
        { "type": "Ring", "id": 0 },
        { "type": "Full Face", "id": 1 }
    ],
    "material": [
        {
            "id": 0,
            "name": "Rubber (Elastomeric)",
            "seatingStress": 500,
            "crushingStress": 1500
        },
        {
            "id": 1,
            "name": "CFG/PTFE",
            "seatingStress": 4800,
            "crushingStress": 15000
        }
    ]
};
//# sourceMappingURL=BoltTorqueRanges.js.map