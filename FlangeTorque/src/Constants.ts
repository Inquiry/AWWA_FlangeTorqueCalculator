﻿const domain = "";
const storeProductBase = "/store/productdetail.aspx?productid=";
const m11_id = 62572078;
const c207_id = 35009101;
const c604_id = 66610737;

enum GasketStyle {
    "Ring",
    "FullFace"
}

enum MaterialName {
    "Rubber",
    "CompositeFiber"
}

enum FlangeClass {
    "B",
    "D",
    "E",
    "F"
}