var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Outputs = (function () {
    function Outputs(calcInput) {
        this.calculateOutputs(calcInput);
    }
    /**
     * Calculates all the output values before being assigned to the sections
     * @param calcInput
     * @param loadOutputs
     */
    Outputs.prototype.calculateOutputs = function (calcInput) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.input = calcInput;
                        this.material = new Material(calcInput.gasketMaterial);
                        this.flange = new Flange(calcInput);
                        this.calculations = new CalculatedValues(this.flange, this.material, calcInput);
                        this.boltLoad = new BoltLoad(this.calculations, calcInput.selectedNutFactor);
                        if (this.material && this.flange && this.calculations && this.boltLoad) {
                            this.loadAtGasketSeating = this.calculatePsi(this.calculations.seatingStress, this.calculations.gasketSeatingFaceArea, this.calculations.numberOfBolts, this.calculations.boltCrossSection);
                            this.loadAtGasketCrushing = this.calculatePsi(this.calculations.crushingStress, this.calculations.gasketSeatingFaceArea, this.calculations.numberOfBolts, this.calculations.boltCrossSection);
                        }
                        else {
                            throw "still not calculating";
                        }
                        return [4 /*yield*/, this.secondCalc()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Outputs.prototype.secondCalc = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ranges;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.torqueAtGasketSeating = this.calculateTorque(this.input.selectedNutFactor, this.calculations.boltSize, this.loadAtGasketSeating);
                        this.torqueAtGasketCrushing = this.calculateTorque(this.input.selectedNutFactor, this.calculations.boltSize, this.loadAtGasketCrushing);
                        this.percentAtSeating = this.loadAtGasketSeating / this.calculations.minYieldStress;
                        this.percentAtCrushing = this.loadAtGasketCrushing / this.calculations.minYieldStress;
                        return [4 /*yield*/, this.getRecommendedTorque()];
                    case 1:
                        ranges = _a.sent();
                        this.maxRecommendedBoltTorque = ranges.max;
                        this.minRecommendedBoltTorque = ranges.min;
                        return [2 /*return*/];
                }
            });
        });
    };
    Outputs.prototype.calcPsiMinMax = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.boltLoadPsiMin = this.getBoltLoadPsiAtTorque(this.minRecommendedBoltTorque, this.input.selectedNutFactor, this.calculations.boltSize);
                this.boltLoadPsiMax = this.getBoltLoadPsiAtTorque(this.maxRecommendedBoltTorque, this.input.selectedNutFactor, this.calculations.boltSize);
                return [2 /*return*/];
            });
        });
    };
    Outputs.prototype.calcLoadPercentMinMax = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.boltLoadPercentMin = this.boltLoadPsiMin / this.calculations.minYieldStress;
                this.boltLoadPercentMax = this.boltLoadPsiMax / this.calculations.minYieldStress;
                return [2 /*return*/];
            });
        });
    };
    Outputs.prototype.calculatePsi = function (stress, gasketSeatingFaceArea, numberOfBolts, boltCrossSection) {
        var psi = gasketSeatingFaceArea * stress / numberOfBolts / boltCrossSection;
        return psi;
    };
    Outputs.prototype.calculateTorque = function (nutFactor, boltDiameter, boltLoading) {
        var torque = nutFactor * boltDiameter * boltLoading / 12.0;
        return torque;
    };
    Outputs.prototype.getRecommendedTorque = function () {
        return __awaiter(this, void 0, void 0, function () {
            var range, minTorque, maximumMinTorque;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        range = { max: 0, min: 0 };
                        if (!!this.boltLoad.highTorque) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.boltLoad];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        range.max = Math.max(this.torqueAtGasketSeating * 1.15, Math.min(this.torqueAtGasketCrushing * 0.9, this.input.selectedNutFactor * this.calculations.boltSize * 1.1 * this.boltLoad.targetPsi / 12.0));
                        range.max = Math.round(range.max / 5.0) * 5.0; //range.max % 5.0 >= 2.5 ? Math.round(range.max / 5.0) * 5.0 + 5.0 : Math.round(range.max / 5.0) * 5.0;
                        minTorque = this.input.selectedNutFactor * this.calculations.boltSize * 0.9 * this.boltLoad.targetPsi / 12.0;
                        maximumMinTorque = this.torqueAtGasketSeating * 1.1;
                        if (Math.max(maximumMinTorque, minTorque) > 0.8 * range.max) {
                            range.min = 0.8 * range.max;
                        }
                        else {
                            range.min = Math.max(maximumMinTorque, minTorque);
                            range.min = Math.round(range.min / 5.0) * 5.0; //range.min % 5.0 >= 2.5 ? Math.round(range.min / 5.0) * 5.0 + 5.0 : Math.round(range.min / 5.0) * 5.0;
                        }
                        return [2 /*return*/, range];
                }
            });
        });
    };
    Outputs.prototype.getBoltLoadPsiAtTorque = function (recommendedTorque, selectedNutFactor, boltSize) {
        return recommendedTorque * 12.0 / selectedNutFactor / boltSize;
    };
    return Outputs;
}());
function formatOutputs(calcOutputs) {
    return __awaiter(this, void 0, void 0, function () {
        var flangeDetails, gasketMaterial, threadLubricant, boltYieldLoad;
        return __generator(this, function (_a) {
            flangeDetails = new OutputSection;
            flangeDetails.sectionId = "fd";
            flangeDetails.values = [
                calcOutputs.calculations.numberOfBolts.toString(),
                calcOutputs.calculations.boltSize.toFixed(3) + " in.",
                new Intl.NumberFormat("en", { style: "decimal" }).format(calcOutputs.calculations.minYieldStress) + " psi",
                calcOutputs.calculations.flangeID.toFixed(2) + " in.",
                calcOutputs.calculations.flangeOD + " in.",
                calcOutputs.calculations.boltCircle.toFixed(2) + " in.",
                calcOutputs.calculations.gasketSeatingFaceArea.toFixed(1) + " in.<sup>2</sup>",
                calcOutputs.calculations.boltCrossSection.toFixed(3) + " in.<sup>2</sup>"
            ];
            gasketMaterial = new OutputSection;
            gasketMaterial.sectionId = "gm";
            gasketMaterial.values = [
                new Intl.NumberFormat("en", { style: "decimal" }).format(calcOutputs.material.SeatingStress) + " psi",
                new Intl.NumberFormat("en", { style: "decimal" }).format(calcOutputs.material.CrushingStress) + " psi"
            ];
            threadLubricant = new OutputSection;
            threadLubricant.sectionId = "tl";
            threadLubricant.values = [
                calcOutputs.input.selectedNutFactor.toString()
            ];
            boltYieldLoad = new OutputSection;
            boltYieldLoad.sectionId = "byl";
            boltYieldLoad.values = [
                new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.loadAtGasketSeating) + " psi",
                new Intl.NumberFormat("en", { style: "percent" }).format(calcOutputs.percentAtSeating),
                new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.loadAtGasketCrushing) + " psi",
                new Intl.NumberFormat("en", { style: "percent" }).format(calcOutputs.percentAtCrushing),
                new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.torqueAtGasketSeating) + " ft-lbs",
                new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.torqueAtGasketCrushing) + " ft-lbs",
            ];
            return [2 /*return*/, [flangeDetails, gasketMaterial, threadLubricant, boltYieldLoad]];
        });
    });
}
var OutputSection = (function () {
    function OutputSection() {
    }
    return OutputSection;
}());
//# sourceMappingURL=Outputs.js.map