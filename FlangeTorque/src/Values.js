var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///Pulls and stores the values from the resource files
var StaticValues = (function () {
    function StaticValues(flange, material) {
        var resourceFile;
        this.seatingStress = material.SeatingStress;
        this.crushingStress = material.CrushingStress;
        if (flange.class === FlangeClass.F) {
            for (var i = 0; i < classF_Values.length; i++) {
                if (flange.nominalPipeSize === classF_Values[i].NominalPipeSize) {
                    resourceFile = classF_Values[i];
                    break;
                }
            }
        }
        else {
            for (var i = 0; i < classBDE_Values.length; i++) {
                if (flange.nominalPipeSize === classBDE_Values[i].NominalPipeSize) {
                    resourceFile = classBDE_Values[i];
                    break;
                }
            }
        }
        if (resourceFile) {
            this.nominalPipeSize = resourceFile.NominalPipeSize;
            this.numberOfBolts = resourceFile.NumberofBolts;
            this.boltSizeDiameter = resourceFile.BoltSizeDiameter;
            this.boltSize = resourceFile.BoltSize;
            this.threadsPerInch = resourceFile.ThreadsperInch;
            this.heavyHexNutSize = resourceFile.HeavyHexNutSize;
            this.minYieldStress = resourceFile.MinYieldStress;
            this.flangeID = flange.seatingFaceId ? flange.seatingFaceId : resourceFile.FlangeID;
            this.flangeOD = resourceFile.FlangeOD;
            this.boltCircle = resourceFile.BoltCircle;
        }
    }
    return StaticValues;
}());
var CalculatedValues = (function (_super) {
    __extends(CalculatedValues, _super);
    function CalculatedValues(flange, material, input) {
        var _this = _super.call(this, flange, material) || this;
        _this.nominalBoltArea = Math.pow((_this.boltSize / 2), 2) * Math.PI;
        _this.boltCircleID = _this.boltCircle - _this.boltSize - 0.125;
        _this.ringArea = Math.pow((_this.boltCircleID / 2), 2) * Math.PI - Math.pow((_this.flangeID / 2), 2) * Math.PI;
        _this.fullArea = Math.pow((_this.flangeOD / 2), 2) * Math.PI - Math.pow((_this.flangeID / 2), 2) * Math.PI;
        _this.ringMin = _this.seatingStress * _this.ringArea / _this.numberOfBolts / _this.boltSize;
        _this.ringMax = _this.crushingStress * _this.ringArea / _this.numberOfBolts / _this.nominalBoltArea;
        _this.ringMinPercent = _this.ringMin / _this.ringMax;
        _this.ringMaxPercent = _this.ringMax / _this.minYieldStress;
        _this.fullMin = _this.seatingStress * _this.fullArea / _this.numberOfBolts / _this.nominalBoltArea;
        _this.fullMax = _this.crushingStress * _this.fullArea / _this.numberOfBolts / _this.nominalBoltArea;
        _this.fullMinPercent = _this.fullMin / _this.fullMax;
        _this.fullMaxPercent = _this.fullMax / _this.minYieldStress;
        _this.boltTension_50percent = _this.minYieldStress * (0.5) * _this.nominalBoltArea;
        if (input.gasketStyle === GasketStyle.Ring) {
            _this.gasketSeatingFaceArea = (Math.pow((_this.boltCircle - _this.boltSize - 0.125), 2) - Math.pow(_this.flangeID, 2)) * Math.PI / 4;
        }
        else {
            _this.gasketSeatingFaceArea = ((Math.pow(_this.flangeOD, 2) - Math.pow(_this.flangeID, 2)) * Math.PI / 4 - _this.numberOfBolts * (Math.pow((0.125 + _this.boltSize), 2) * Math.PI / 4));
        }
        _this.boltCrossSection = Math.pow((_this.boltSize / 2), 2) * Math.PI;
        return _this;
    }
    //Calculate the Torque for the bolt yield percent and K factor
    CalculatedValues.prototype.getGasketPercent = function (percentBoltYield, selectedNutFactor) {
        if (percentBoltYield > 1) {
            var err = "Percent bolt yield cannot be greater than 1";
            throw err;
        }
        var gasketPercent = this.minYieldStress * this.nominalBoltArea / 12 * percentBoltYield * selectedNutFactor;
        if (!gasketPercent) {
            throw "There was an error in calculating gasketPercent. Not all values present";
        }
        //round to the nearest 5%
        gasketPercent = gasketPercent * 100;
        gasketPercent = gasketPercent % 5.0 >= 2.5 ? Math.round(gasketPercent / 5.0) * 5.0 + 5.0 : Math.round(gasketPercent / 5.0) * 5.0;
        gasketPercent = gasketPercent / 100;
        return gasketPercent;
    };
    return CalculatedValues;
}(StaticValues));
//# sourceMappingURL=Values.js.map