﻿
//Contains all the variables and calculations for a gasket material type
class Material {
    SeatingStress: number;
    CrushingStress: number;
    Name: string;

    constructor(materialTypeId: number) {

        //Get the seating and crushing strength based on the material type        
        for (let i = 0; i < gasketDetails.material.length; i++) {
            if (gasketDetails.material[i].id === materialTypeId) {
                this.SeatingStress = gasketDetails.material[i].seatingStress;
                this.CrushingStress = gasketDetails.material[i].crushingStress;
                this.Name = MaterialName[materialTypeId];
                break;
            }
        }
    }
}