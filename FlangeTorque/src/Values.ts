﻿///Pulls and stores the values from the resource files
class StaticValues implements IStaticValues {
    //public properties
    readonly nominalPipeSize: number;
    readonly numberOfBolts: number;
    readonly boltSizeDiameter: string;
    readonly boltSize: number;
    readonly threadsPerInch: number;
    readonly heavyHexNutSize: string;
    readonly minYieldStress: number;
    readonly flangeID: number;
    readonly flangeOD: number;
    readonly boltCircle: number;
    readonly seatingStress: number;
    readonly crushingStress: number;

    constructor(flange: Flange, material: Material) {
        var resourceFile;

        this.seatingStress = material.SeatingStress;
        this.crushingStress = material.CrushingStress;

        if (flange.class === FlangeClass.F) { //check through the appropriate resource file for the key we want
            for (let i = 0; i < classF_Values.length; i++) {
                if (flange.nominalPipeSize === classF_Values[i].NominalPipeSize) {
                    resourceFile = classF_Values[i];
                    break;
                }
            }
        } else {
            for (let i = 0; i < classBDE_Values.length; i++) {
                if (flange.nominalPipeSize === classBDE_Values[i].NominalPipeSize) {
                    resourceFile = classBDE_Values[i];
                    break;
                }
            }
        }

        if (resourceFile) { //I was able to use regex find and replace to add all of these. Woo!
            this.nominalPipeSize = resourceFile.NominalPipeSize;
            this.numberOfBolts = resourceFile.NumberofBolts;
            this.boltSizeDiameter = resourceFile.BoltSizeDiameter;
            this.boltSize = resourceFile.BoltSize;
            this.threadsPerInch = resourceFile.ThreadsperInch;
            this.heavyHexNutSize = resourceFile.HeavyHexNutSize;
            this.minYieldStress = resourceFile.MinYieldStress;
            this.flangeID = flange.seatingFaceId ? flange.seatingFaceId : resourceFile.FlangeID;
            this.flangeOD = resourceFile.FlangeOD;
            this.boltCircle = resourceFile.BoltCircle;
        }
    }
}

class CalculatedValues extends StaticValues implements ICalculatedValues {
    //public properties
    readonly nominalBoltArea: number;
    readonly boltCircleID: number;
    readonly ringArea: number;
    readonly fullArea: number;
    readonly ringMin: number;    
    readonly ringMax: number;
    readonly ringMinPercent: number;
    readonly ringMaxPercent: number;
    readonly fullMin: number;    
    readonly fullMax: number;
    readonly fullMinPercent: number;
    readonly fullMaxPercent: number;
    readonly boltTension_50percent: number;
    readonly gasketSeatingFaceArea: number;
    readonly boltCrossSection: number;

    constructor(flange: Flange, material: Material, input: ICalculatorInputs) {
        super(flange, material);

        this.nominalBoltArea = Math.pow((this.boltSize / 2),2) * Math.PI;
        this.boltCircleID = this.boltCircle - this.boltSize - 0.125;
        this.ringArea = Math.pow((this.boltCircleID / 2), 2) * Math.PI - Math.pow((this.flangeID / 2), 2) * Math.PI;
        this.fullArea = Math.pow((this.flangeOD / 2), 2) * Math.PI - Math.pow((this.flangeID / 2), 2) * Math.PI;
        this.ringMin = this.seatingStress * this.ringArea / this.numberOfBolts / this.boltSize;
        this.ringMax = this.crushingStress * this.ringArea / this.numberOfBolts / this.nominalBoltArea;
        this.ringMinPercent = this.ringMin / this.ringMax;
        this.ringMaxPercent = this.ringMax / this.minYieldStress;
        this.fullMin = this.seatingStress * this.fullArea / this.numberOfBolts / this.nominalBoltArea;
        this.fullMax = this.crushingStress * this.fullArea / this.numberOfBolts / this.nominalBoltArea;
        this.fullMinPercent = this.fullMin / this.fullMax;
        this.fullMaxPercent = this.fullMax / this.minYieldStress;
        this.boltTension_50percent = this.minYieldStress * (0.5) * this.nominalBoltArea;


        if (input.gasketStyle === GasketStyle.Ring) {
            this.gasketSeatingFaceArea = (Math.pow((this.boltCircle - this.boltSize - 0.125), 2) - Math.pow(this.flangeID, 2)) * Math.PI / 4;
        } else {
            this.gasketSeatingFaceArea = ((Math.pow(this.flangeOD, 2) - Math.pow(this.flangeID, 2)) * Math.PI / 4 - this.numberOfBolts * (Math.pow((0.125 + this.boltSize), 2) * Math.PI / 4));
        }

        this.boltCrossSection = Math.pow((this.boltSize / 2), 2) * Math.PI;
    }

    //Calculate the Torque for the bolt yield percent and K factor
    getGasketPercent (percentBoltYield: number, selectedNutFactor: number): number {
        if (percentBoltYield > 1) {
            var err = "Percent bolt yield cannot be greater than 1";
            throw err;            
        }

        let gasketPercent = this.minYieldStress * this.nominalBoltArea / 12 * percentBoltYield * selectedNutFactor;

        if (!gasketPercent) {
            throw "There was an error in calculating gasketPercent. Not all values present";
        }

        //round to the nearest 5%
        gasketPercent = gasketPercent * 100;
        gasketPercent = gasketPercent % 5.0 >= 2.5 ? Math.round(gasketPercent / 5.0) * 5.0 + 5.0 : Math.round(gasketPercent / 5.0) * 5.0;
        gasketPercent = gasketPercent / 100;
        return gasketPercent;
    }
}


