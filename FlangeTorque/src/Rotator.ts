﻿async function fillRotator() {
    let linkedProductDiv = $(".linked-products")[0] as HTMLDivElement;

    //for (let i = 0; i < activeProducts.length; i++) {

    //    linkedProductDiv.innerHTML += `<div class="linked-product">` +
    //        `<a href="${activeProducts[i].url}">` +
    //        `<img src="images/relatedProducts/${activeProducts[i].image}" alt="${activeProducts[i].title}">` +
    //        `<p style="color:inherit;">${activeProducts[i].title}</p>` +
    //        `</a>` +
    //        `<p>LIST PRICE: <span>${activeProducts[i].listPrice}</span></p>` +
    //        `<p>MEMBER PRICE: <span class="member-price">${activeProducts[i].memberPrice}</span></p>` +
    //        `</div>`;
    //}

    for (let i = 0; i < activeProductIds.length; i++) {
        let product = new Product(activeProductIds[i]);

        if (!product.title) continue; //no need to populate something if no info is grabbed

        linkedProductDiv.innerHTML += `<div class="linked-product">` +
            `<a href="${product.url}">` +
            `<img src="images/relatedProducts/${product.image}" alt="${product.title}">` +
            `<p style="color:inherit;">${product.title}</p>` +
            `</a>` +
            `<p>LIST PRICE: <span>${product.listPrice}</span></p>` +
            `<p>MEMBER PRICE: <span class="member-price">${product.memberPrice}</span></p>` +            
        `</div>`;
    }
}

/**
 * Scrapes an url for product metadata in order to populate itself
 */
class Product {
    private _listPrice: number;
    private _memberPrice: number;    
    public title: string;
    public url: string;
    public image: string;

    public get listPrice(): string {
        return new Intl.NumberFormat("en", { style: "currency", currency: "USD"}).format(this._listPrice);
    }
    public set listPrice(amount: string) {
        amount = amount.replace(/\D/g, "");
        this._listPrice = parseFloat(amount) ? parseFloat(amount) : 0.0;        
    }

    public get memberPrice(): string {
        return new Intl.NumberFormat("en", { style: "currency", currency: "USD"}).format(this._memberPrice);
    }
    public set memberPrice(amount: string) {
        amount = amount.replace(/^\D/, "");
        this._memberPrice = parseFloat(amount) ? parseFloat(amount) : 0.0;
    }

    constructor(productId: number) {

        this.url = domain + storeProductBase + productId;

        //using jQuery because that was the first thing I could find
        $.get(this.url, function (data: string) {
            this.title = $(data).find("meta[property=og:title]").attr("content");
            this.image = $(data).find("meta[property=og:image]").attr("content");
            this.listPrice = $(data).find("meta[property=og:price]").attr("content");
            this.memberPrice = $(data).find("span[id*=ProductINVBuyBoxControl_PriceDisplayControl1_MemberPrice_PriceValueLabel").val();
        });
    }
}