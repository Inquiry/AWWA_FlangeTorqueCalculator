﻿
class Flange {
    class: FlangeClass;
    nominalPipeSize: number;
    //boltSize: number;
    seatingFaceId?: number;

    constructor(input: ICalculatorInputs) {
        this.nominalPipeSize = input.nominalFlangeDiameter;
        this.class = input.flangeClass;
        if (input.finishedFlangeSeatingFaceId)
            this.seatingFaceId = input.finishedFlangeSeatingFaceId;
    }
}