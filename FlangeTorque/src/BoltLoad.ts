﻿class BoltLoad {
    //Public properties
    lowPsi: number;
    targetPsi: number;
    highPsi: number;
    excessivePsi: number;
    yieldPsi: number;

    lowTorque: number;
    targetTorque: number;
    highTorque: number;
    excessiveTorque: number;
    yieldTorque: number;

    constructor(staticValues: IStaticValues, kValue: number) {
        //all the torque values rely on PSI being calculated first
        this.calculateTorqueRanges(staticValues, kValue);
    }

    private async calculatePsi(staticValues: IStaticValues) {
        let testLow = this.BoltLoadPsi(0.2, staticValues.minYieldStress);

        this.lowPsi = (testLow < 25000) ? 25000 : testLow;
        this.targetPsi = this.BoltLoadPsi(0.5, staticValues.minYieldStress);
        this.highPsi = this.BoltLoadPsi(0.7, staticValues.minYieldStress);
        this.excessivePsi = this.BoltLoadPsi(0.9, staticValues.minYieldStress);
        this.yieldPsi = this.BoltLoadPsi(1, staticValues.minYieldStress);
    }

    private async calculateTorqueRanges(staticValues: IStaticValues, kValue: number) {
        await this.calculatePsi(staticValues);
        this.lowTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.lowPsi);
        this.targetTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.targetPsi);
        this.highTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.highPsi);
        this.excessiveTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.excessivePsi);
        this.yieldTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.yieldPsi);
    }


    private BoltLoadPsi(percentage: number, minYieldStress: number): number {
        if (percentage > 1 || percentage < 0) {
            throw "Percentage not within expected range. Must be between 0 and 1";
        }

        return percentage * minYieldStress;
    }

    private BoltLoadTorque(kValue: number, boltSize: number, psi: number): number {
        if (!psi) {
            throw "PSI has not be calculated yet"; //TODO: remove this before deployment. Callback may be needed
        }

        return kValue * boltSize * psi / 12;
    }
}