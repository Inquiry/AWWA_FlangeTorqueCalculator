//Contains all the variables and calculations for a gasket material type
var Material = (function () {
    function Material(materialTypeId) {
        //Get the seating and crushing strength based on the material type        
        for (var i = 0; i < gasketDetails.material.length; i++) {
            if (gasketDetails.material[i].id === materialTypeId) {
                this.SeatingStress = gasketDetails.material[i].seatingStress;
                this.CrushingStress = gasketDetails.material[i].crushingStress;
                this.Name = MaterialName[materialTypeId];
                break;
            }
        }
    }
    return Material;
}());
//# sourceMappingURL=Material.js.map