var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
function loadClassDropDown() {
    var flangeClassDropDown = document.getElementById("flangeClassDropDown");
    for (var i = 0; i < flangeClasses.length; i++) {
        var option = document.createElement("option");
        option.text = flangeClasses[i].class;
        option.value = flangeClasses[i].id.toString();
        flangeClassDropDown.add(option);
    }
    flangeClassDropDown.selectedIndex = -1;
}
/**
 * Holds the value of the currently selected class between changes
 */
var currentlySelectedPipeClass = -1;
function loadPipeSizeDropDown() {
    var newPipeClassId = parseInt(document.getElementById("flangeClassDropDown").value);
    var isDiameterListTheSame = true;
    //checks to see if a change is needed to the diameter dropdown
    if ((currentlySelectedPipeClass != FlangeClass.F && newPipeClassId === FlangeClass.F)
        || (currentlySelectedPipeClass === FlangeClass.F && newPipeClassId != FlangeClass.F)
        || newPipeClassId >= 0) {
        isDiameterListTheSame = false;
    }
    //ensures each new selection can be checked
    currentlySelectedPipeClass = newPipeClassId;
    if (isDiameterListTheSame) {
        return;
    }
    var diameterDropDown = document.getElementById("flangeDiameterDropDown");
    var activeFlangeValues = [];
    //the nominal pipe diameters only change on the F class. I don't know why
    if (newPipeClassId) {
        activeFlangeValues = classF_Values;
    }
    else {
        activeFlangeValues = classBDE_Values;
    }
    if (diameterDropDown.length > 0) {
        while (diameterDropDown.length > 0) {
            diameterDropDown.remove(0);
        }
    }
    for (var i = 0; i < activeFlangeValues.length; i++) {
        var option = document.createElement("option");
        option.text = activeFlangeValues[i].NominalPipeSize.toString();
        option.value = activeFlangeValues[i].NominalPipeSize.toString();
        diameterDropDown.add(option);
    }
    diameterDropDown.selectedIndex = -1;
}
function loadGasketDetails() {
    var gasketStyleDropDown = document.getElementById("gasketStyleDropDown");
    var gasketMaterialDropDown = document.getElementById("gasketMaterialDropDown");
    for (var i = 0; i < gasketDetails.style.length; i++) {
        var option = document.createElement("option");
        option.text = gasketDetails.style[i].type;
        option.value = gasketDetails.style[i].id.toString();
        gasketStyleDropDown.add(option);
    }
    for (var i = 0; i < gasketDetails.material.length; i++) {
        var option = document.createElement("option");
        option.text = gasketDetails.material[i].name;
        option.value = gasketDetails.material[i].id.toString();
        gasketMaterialDropDown.add(option);
    }
    gasketStyleDropDown.selectedIndex = -1;
    gasketMaterialDropDown.selectedIndex = -1;
}
/**
 * Required in order to wait for the class dropdown to load first
 */
function loadFlangeDetails() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, loadClassDropDown()];
                case 1:
                    _a.sent();
                    loadPipeSizeDropDown();
                    return [2 /*return*/];
            }
        });
    });
}
function loadInputs() {
    var inputs;
    inputs = { flangeClass: 0, nominalFlangeDiameter: 0, finishedFlangeSeatingFaceId: 0, gasketStyle: 0, gasketMaterial: 0, selectedNutFactor: 0 };
    //flange details
    inputs.flangeClass = parseInt(document.getElementById("flangeClassDropDown").value);
    inputs.nominalFlangeDiameter = parseInt(document.getElementById("flangeDiameterDropDown").value);
    var faceId = document.getElementById("flangeSeatingFace").value.trim();
    inputs.finishedFlangeSeatingFaceId = (!faceId || faceId === "") ? undefined : parseFloat(faceId);
    //gasket details
    inputs.gasketStyle = parseInt(document.getElementById("gasketStyleDropDown").value);
    inputs.gasketMaterial = parseInt(document.getElementById("gasketMaterialDropDown").value);
    //selected nut factor, K
    var k = parseFloat(document.getElementById("selectedNutValue").value.trim());
    inputs.selectedNutFactor = (!k) ? 0.0 : k;
    return inputs;
}
;
function displayFlangeTorqueCalculations(out) {
    return __awaiter(this, void 0, void 0, function () {
        var _a, i, j, field;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!(out.torqueAtGasketSeating > out.boltLoad.highTorque)) return [3 /*break*/, 1];
                    $("#targetTorque_From").text("Error!");
                    $("#targetTorque_To").text("Error!");
                    $("#boltLoadPsi_From").text("Error!");
                    $("#boltLoadPsi_To").text("Error!");
                    $("#boltLoadPercent_From").text("Error!");
                    $("#boltLoadPercent_To").text("Error!");
                    return [3 /*break*/, 4];
                case 1:
                    $("#targetTorque_From").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.minRecommendedBoltTorque) + " ft-lbs");
                    $("#targetTorque_To").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.maxRecommendedBoltTorque) + " ft-lbs");
                    return [4 /*yield*/, out.calcPsiMinMax()];
                case 2:
                    _b.sent();
                    $("#boltLoadPsi_From").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.boltLoadPsiMin) + " psi");
                    $("#boltLoadPsi_To").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.boltLoadPsiMax) + " psi");
                    return [4 /*yield*/, out.calcLoadPercentMinMax()];
                case 3:
                    _b.sent();
                    $("#boltLoadPercent_From").text(new Intl.NumberFormat("en", { style: "percent" }).format(out.boltLoadPercentMin));
                    $("#boltLoadPercent_To").text(new Intl.NumberFormat("en", { style: "percent" }).format(out.boltLoadPercentMax));
                    _b.label = 4;
                case 4:
                    _a = out;
                    return [4 /*yield*/, formatOutputs(out)];
                case 5:
                    _a.outputSections = _b.sent();
                    for (i = 0; i < out.outputSections.length; i++) {
                        for (j = 0; j < out.outputSections[i].values.length; j++) {
                            field = document.getElementById(out.outputSections[i].sectionId + (j + 1));
                            if (field)
                                field.innerHTML = out.outputSections[i].values[j];
                        }
                    }
                    checkForWarnings(out);
                    return [2 /*return*/];
            }
        });
    });
}
function changeMaterialLabel() {
    $("#material-label").text(function () {
        var material = document.getElementById("gasketMaterialDropDown");
        return material.options[material.selectedIndex] ? material.options[material.selectedIndex].innerHTML : "No";
    });
}
/**
 * The main. process for calculating all the flange-torque values
 */
function calculateFtOutputs() {
    return __awaiter(this, void 0, void 0, function () {
        var inputs, calculations;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, loadInputs()];
                case 1:
                    inputs = _a.sent();
                    return [4 /*yield*/, new Outputs(inputs)];
                case 2:
                    calculations = _a.sent();
                    return [2 /*return*/, calculations];
            }
        });
    });
}
function init() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    loadGasketDetails();
                    return [4 /*yield*/, loadFlangeDetails()];
                case 1:
                    _a.sent();
                    isFormFullyFilled();
                    return [2 /*return*/];
            }
        });
    });
}
function checkForWarnings(calcValues) {
    var warningLabels = document.getElementsByClassName("selection-warning");
    for (var i = 0; i < warningLabels.length; i++) {
        warningLabels[i].setAttribute("style", "display: none");
        warningLabels[i].innerHTML = "";
        switch (i) {
            case 0: {
                if (calcValues.input.finishedFlangeSeatingFaceId < calcValues.input.nominalFlangeDiameter
                    || calcValues.input.finishedFlangeSeatingFaceId > (calcValues.input.nominalFlangeDiameter + 2 + calcValues.input.nominalFlangeDiameter / 100)) {
                    warningLabels[i].setAttribute("style", "display: block");
                }
                break;
            }
            case 1: {
                if (calcValues.input.gasketStyle === GasketStyle.FullFace) {
                    if (calcValues.input.nominalFlangeDiameter > 24 || calcValues.flange.class === FlangeClass.F) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="fullFace_warning" class="warning-message">Full Face Gaskets are not allowed in AWWA C207 for Class F or nom. ' +
                            'diameters over 24”, bolt yield can be exceeded before obtaining gasket seating stress</p>';
                        //document.getElementById("fullFace_warning").style.display = "block";
                    }
                }
                if (calcValues.material.Name.toLowerCase() == "rubber") {
                    if (calcValues.flange.class === FlangeClass.E || calcValues.flange.class === FlangeClass.F) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="rubber_warning" class="warning-message">AWWA C207 does not allow Elastomeric (Rubber) Ring Gaskets for Class F, ' +
                            'or Class E; over 24 in., over 175 psi up to 12 in., over 150 psi greater than 12 in. up to 24 in. </p>';
                        //document.getElementById("rubber_warning").style.display = "block";
                    }
                }
                break;
            }
            case 2: {
                if (calcValues.torqueAtGasketSeating > calcValues.boltLoad.highTorque) {
                    warningLabels[i].setAttribute("style", "display: block");
                    warningLabels[i].innerHTML += '<p id="seatingStress_warning" class="warning-message" > Gasket seating stress develops a ' +
                        'required load in excess of the allowable bolt load, change the gasket</p>';
                }
                if (calcValues.torqueAtGasketSeating < calcValues.boltLoad.highTorque) {
                    if (calcValues.maxRecommendedBoltTorque > calcValues.boltLoad.highTorque
                        && calcValues.maxRecommendedBoltTorque <= calcValues.boltLoad.excessiveTorque) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="overstress_warning" class="warning-message">Select a value from the middle of ' +
                            'recommended target torque range to reduce possible bolt overstress.</p>';
                    }
                    if (calcValues.minRecommendedBoltTorque < calcValues.boltLoad.lowTorque) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="crushing_warning" class="warning-message">Due to potential gasket crushing, torque values are below recommended bolt loading ' +
                            'for a stable joint.<br />Selection of a different gasket material may be necessary.</p>';
                    }
                }
                break;
            }
        }
    }
}
//the logic for recalculating on input change
function recalculateValues() {
    return __awaiter(this, void 0, void 0, function () {
        var newCalculation;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!isFormFullyFilled()) return [3 /*break*/, 2];
                    return [4 /*yield*/, calculateFtOutputs()];
                case 1:
                    newCalculation = _a.sent();
                    displayFlangeTorqueCalculations(newCalculation);
                    return [3 /*break*/, 3];
                case 2:
                    window.alert("One or more required fields still needs to be filled");
                    _a.label = 3;
                case 3: return [2 /*return*/];
            }
        });
    });
}
function isFormFullyFilled() {
    var isFilled = true;
    //ensures all fields are populated properly before attempting a calculation
    var requiredFields = document.getElementsByClassName("required");
    for (var i = 0; i < requiredFields.length; i++) {
        if (requiredFields[i].hasAttribute("selectedIndex")) {
            if (requiredFields[i].selectedIndex === -1) {
                isFilled = false;
                requiredFields[i].className += " alert-danger";
            }
            else {
                requiredFields[i].className = "required calc-inputs";
            }
        }
        else {
            if (requiredFields[i].value.trim() === "") {
                isFilled = false;
                requiredFields[i].className += " alert-danger";
            }
            else {
                requiredFields[i].className = "required calc-inputs";
            }
        }
    }
    return isFilled;
}
function loadLinks() {
    $(".m11-17").attr("href", domain + storeProductBase + m11_id);
    $(".c207-13").attr("href", domain + storeProductBase + c207_id);
    $(".c604-17").attr("href", domain + storeProductBase + c604_id);
    $("#contactUsButton").attr("href", domain + "/about-us/contact-us.aspx");
    $("#backToCommunityButton").attr("href", domain + "/resources-tools/water-knowledge.aspx");
    $("#instructionButton").attr("href", "/FTCResources/UserInstructions.docx");
    $("#awwaLogo").attr("href", domain);
}
//binds enter key to calculate values
$(function () {
    $(document).on("keypress", function (e) {
        if (e.which == 13) {
            recalculateValues();
        }
    });
});
/**
 * Exactly what it says on the tin: Clears the calculator's inputs for a new calculation
 */
function clearInputs() {
    var calcInputs = document.getElementsByClassName("calc-inputs");
    for (var i = 0; i < calcInputs.length; i++) {
        if (calcInputs[i].hasAttribute("selectedIndex")) {
            calcInputs[i].selectedIndex = -1;
        }
        else {
            calcInputs[i].value = "";
        }
    }
    var diameterDropDown = document.getElementById("flangeDiameterDropDown");
    if (diameterDropDown.length > 0) {
        while (diameterDropDown.length > 0) {
            diameterDropDown.remove(0);
        }
    }
    $("#targetTorque_From").text("");
    $("#targetTorque_To").text("");
    $("#boltLoadPsi_From").text("");
    $("#boltLoadPsi_To").text("");
    $("#boltLoadPercent_From").text("");
    $("#boltLoadPercent_To").text("");
    $(".calc-value-box").text("");
    $(".selection-warning").css("display", "none");
    isFormFullyFilled();
}
window.onload = function () {
    init();
    changeMaterialLabel();
    //fillRotator(); //not needed for DNN
    loadLinks();
};
//# sourceMappingURL=app.js.map