﻿/// <binding AfterBuild='default' Clean='clean' />
/*
This file is the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/
"use strict";

var gulp = require("gulp"),
    del = require("del"),
    less = require("gulp-less"),
    ts = require("gulp-typescript"),
    uglify = require("gulp-uglify"),
    concat = require("gulp-concat");

var paths = {
    webroot: "./wwwroot/",
    src: "./src/"    
};

paths.js = paths.src + "**/*.js";
paths.css = paths.webroot + "css/**/*.css";
paths.ts = paths.src + "**/*.ts";
paths.less = paths.src + "**/*.less";
paths.resources = paths.src + "resources/*.js";
//paths.jquery = "node_modules/jquery/dist/**/*.min.*";
//paths.bootstrap = "node_modules/bootstrap/dist/**/*.*";
//paths.tether = "node_modules/tether/dist/**/*.min.*";
//paths.glyphicons = "node_modules/glyphicons/*.js";

paths.dnn = "./../FlangeTorqueCalc/";

gulp.task("compile-less", function () {
    gulp.src(paths.less).pipe(less()).pipe(gulp.dest(paths.webroot));
    gulp.src(paths.src + "**/*.css").pipe(gulp.dest(paths.webroot));
});

gulp.task("clean", function () {
    return del(["wwwroot/**/*.js", "wwwroot/**/*.ts"]);
});

gulp.task("default", ["concat", "compile-less"], function () {
    //gulp.src([paths.bootstrap, paths.tether]).pipe(gulp.dest(paths.webroot));
    //gulp.src(paths.jquery).pipe(gulp.dest(paths.webroot + "js/"));
});


gulp.task("concat", function () {
    return gulp.src(paths.js)
        .pipe(concat("appFull.js"))
        .pipe(gulp.dest(paths.webroot + "js/"));
});