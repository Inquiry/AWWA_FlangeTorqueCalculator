﻿<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Flange Torque Calculator</title>

    <!--I really hate coding for IE. This shim and polyfill lets IE recognize promises - Sean-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/3.4.0/es5-shim.min.js"></script>
    <script src="https://www.promisejs.org/polyfills/promise-6.1.0.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/appFull.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" media="screen" href="css/CalculatorStyles.css" type="text/css" />
    <link rel="stylesheet" media="print" href="css/PrintResults.css" type="text/css" />
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=59a058e789ce4100138ae35b&product=sticky-share-buttons"></script>


    <!--ShareThis Metadata-->
    <!--Facebook-->
    <meta property="og:url" content="https://testawwa.org/resources-tools/FlangeTorqueCalculator" />
    <meta property="og:type" content="tool" />
    <meta property="og:title" content="Flange Torque Stress Calculator" />
    <meta property="og:description" content="Based on the M11 C207 specification sheet" />
    <meta property="og:image" content="" />

    <!--Twitter-->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@awwaace" />
    <meta name="twitter:title" content="M11 Flange Torque Calculator" />
    <meta name="twitter:description" content="A tool for calculating the optimal torque range on flange bolts" />
    <meta name="twitter:image" content="" />

    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip({ html: true });
        })
    </script>
</head>
<body>
    <div class="calc-form">

        <div class="page-header">
            <nav class="navbar mobile">
                <a class="navbar-toggler" href="#" data-toggle="collapse" data-target="#awwa_navbar" aria-controls="awwa_navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <img id="mobile_logo" src="images/mobile_logo.png" />
                </a>
                <div class="collapse navbar-collapse" id="awwa_navbar">
                    <ul class="navbar-nav">
                        <li class="nav-item nav-link"><a href="#">Back To Resource Community</li>
                        <li class="nav-item nav-link"><a href="#">Definitions</li>
                        <li class="nav-item nav-link"><a href="#">Contact Us</li>                        
                    </ul>
                </div>
                <div class="titleSection">
                    <h5>AWWA Flange Joint Bolt Load and Torque Analysis</h5>
                    <div class="supporting-doc-label">
                        <span id="toolVersion">Rev:&nbsp;<span id="versionNumber">0.0</span></span>
                        |
                        <span>Based on:</span>&nbsp;
                        <span>
                            <a href="#">AWWA M11 - 17,</a>
                            <a href="#">AWWA C207 - 13,</a>
                            <a href="#">AWWA C604 - 17</a>
                        </span>
                    </div>
                </div>
            </nav>
            <div class="container calc-header desktop">
                <div class="row">
                    <div class="awwa-logo col-sm-2">
                        <a href="/"><img id="awwaLogo" src="/images/AWWALogo.png" alt="AWWA.org" width="230" height="230" /></a>
                    </div>
                    <div class="title-and-social-share col-sm-10">
                        <div class="sharethis-inline-share-buttons"></div>
                        <br />
                        <div class="titleSection">
                            <h1>AWWA Flange Joint Bolt Load and Torque Analysis</h1>
                            <div class="supporting-doc-label">
                                <span id="toolVersion">Rev:&nbsp;<span id="versionNumber">0.0</span></span>
                                <br />
                                <span id="referenceDocumentLinks">Based on:</span>&nbsp;
                                <span id="supportingDocuments">
                                    <a href="#">AWWA M11 - 17,</a>
                                    <a href="#">AWWA C207 - 13,</a>
                                    <a href="#">AWWA C604 - 17</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="navigation-buttons col-sm-12">
                        <input type="button" id="backToCommunityButton" value="Back To Resource Community" onclick="" />
                        <input type="button" id="definitionsButton" value="Definitions" onclick="" />
                        <input type="button" id="contactUsButton" value="Contact Us" onclick="" />
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="instructionalBox col-sm-12" id="toolInstructions">
                <!--TODO: Import modular HTML editor or makes this read from a resource file-->
                <!--Default Instructions-->
                <ol id="calcInstructions">
                    <li>Select "Flange Class" and "Nominal Flange Diameter"</li>
                    <li>If the finished flange ID is known, enter it otherwise leave box blank (ID assumed for calculations is shown)</li>
                    <li>Select "Gasket Style" and "Gasket Material"</li>
                    <li>Input nut factor (based on lubricant friction factor)</li>
                    <!--<li>Click "Calculate" then follow recommended ranges</li>-->
                    <li>The values will update after each selection</li>
                </ol>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="form-field col-sm-4 form-group" id="calculation_Inputs">
                    <h4>Calculation Input Information</h4>
                    <form class="flange-details">
                        <h5 class="form-section-head">Flange Details</h5>
                        <div id="flange_Details">
                            <span>
                                Flange Class:
                                <span data-toggle="tooltip" title="Select C207 Flange Class Select B, D, E, or F Class Flange" ><span class="glyphicon glyphicon-question-sign"></span></span>                                
                                </span><br />
                            <select id="flangeClassDropDown" onchange="loadPipeSizeDropDown()"></select>
                            <br />
                            <span>
                                Nominal Flange Diameter(in):
                                <span data-toggle="tooltip" title="Select Diameter"><span class="glyphicon glyphicon-question-sign"></span></span>
                                </span><br />
                            <select id="flangeDiameterDropDown" onchange="recalculateValues()"></select><br />
                            <span>
                                Finished Flange Seating Face ID(in):
                                <span id="seatingFace_ToolTip"
                                      data-toggle="tooltip" title="Leave blank unless finished Flange ID is known">
                                    <span class="glyphicon glyphicon-question-sign"></span>
                                </span>
                            </span><br />
                            <input id="flangeSeatingFace" type="text" onchange="recalculateValues()" />
                        </div>
                        <div id="gasket_Details">
                            <h5 class="form-section-head">Gasket Details</h5>
                            <span>
                                Gasket Style: 
                                <span data-toggle="tooltip" title="Select ring or full face gasket.<br> NOTE C207 only allows Full Face for Class B and D 24&quot; and less"><span class="glyphicon glyphicon-question-sign"></span></span>
                            </span>
                            <select id="gasketStyleDropDown" onchange="recalculateValues()"></select><br />
                            <span>
                                Gasket Material: <span data-toggle="tooltip" title="Select Rubber (Elastomeric) or CFG/PTFE for compress fiber"><span class="glyphicon glyphicon-question-sign"></span></span>
                            </span>
                            <select id="gasketMaterialDropDown" onchange="recalculateValues(); changeMaterialLabel()"></select>
                        </div>
                        <div id="thread_Lubricant">
                            <h5 class="form-section-head">
                                Thread Lubricant
                                <span data-toggle="tooltip"
                                      title="Metallic based (anti seize) lubricants can creat a galvanic cell.">
                                    <span class="glyphicon glyphicon-question-sign"></span>
                                </span>
                            </h5>
                            <div class="font-italic">
                                <p>
                                    <span style="font-style:normal">
                                        Selected Nut Factor, K:
                                    </span>
                                    K can be calculated by adding 0.04 to the lubricant friction factor (m + 0.04))
                                </p>
                                <span>Suggested Nut Factors</span>
                                <table class=" indent">
                                    <tr>
                                        <td>0.19</td>
                                        <td>Petroleum Based (SAE 20)</td>
                                    </tr>
                                    <tr>
                                        <td>0.21</td>
                                        <td>Machine Oil</td>
                                    </tr>
                                    <tr>
                                        <td>0.30</td>
                                        <td>Dry (highly variable)</td>
                                    </tr>
                                </table>
                            </div>

                            <input type="text" name="selectedNutValue" id="selectedNutValue" value="0.19" onchange="recalculateValues()" />
                            <br />
                            <!--<input type="button" id="calculateButton" value="Calculate" onclick="" />-->
                            <a target="_blank" href="#" id="printButton" class="primaryButton1" onclick="window.print()">Print</a>

                        </div>
                    </form>
                </div>
                <div class="form-field col-sm-4">
                    <h4>Recommended Ranges</h4>
                    <div class="recommended-ranges-form form">
                        <h5 class="form-section-head">Target Torque</h5>
                        <div class="ranges">
                            <div class="inline">From <span id="targetTorque_From"></span></div>
                            <div class="inline">To <span id="targetTorque_To"></span></div>
                        </div>
                        <h5 class="form-section-head">Bolt Load at Torque</h5>
                        <div class="ranges">
                            <div class="inline">
                                From <span id="boltLoadPsi_From"></span>
                            </div>
                            <div class="inline">
                                To <span id="boltLoadPsi_To"></span>
                            </div>
                            <br />
                                <div class="inline">
                                    From <span id="boltLoadPercent_From"></span>
                                </div>
                                <div class="inline">
                                    To <span id="boltLoadPercent_To"></span>
                                </div>
                            </div>
                            <ol>
                                <li>Select a Torque value within the recommended range and keep all botls within 10% of that value</li>
                                <li>Follow flange installation and assembly guidance found in AWWA M11 and AWWA c604.</li>
                                <li>Follow the flange bolting pattern found in AWWA C604</li>
                            </ol>
                            <div>
                                <img id="torqueFormula" src="images/formula.png" />
                                <ul>
                                    <li>T = Torque (ft-lbs)</li>
                                    <li>K = Nut Factor</li>
                                    <li>D<sub>S</sub> = Doblt Diameter (in)</li>
                                    <li>F = Bolt Stress (psi)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-field col-sm-4">
                        <h4>Calculation Output Data</h4>
                        <div class="output-form">
                            <h5 class="form-section-head">Flange Details</h5>
                            <table class="table table-striped" id="output_flangeDetail">
                                <tr><td>Number of Bolts</td><td class="output-values"><span class="calc-value-box output_fd" id="fd1"></span></td></tr>
                                <tr><td>Bolt Diameter, Ds (in)</td><td class="output-values"><span class="calc-value-box output_fd" id="fd2"></span></td></tr>
                                <tr><td>Bolt Yield Strength (psi)</td><td class="output-values"><span class="calc-value-box output_fd" id="fd3"></span></td></tr>
                                <tr><td>Flange Face ID used for Calculation (in)</td><td class="output-values"><span class="calc-value-box output_fd" id="fd4"></span></td></tr>
                                <tr><td>Flange OD (in)</td><td class="output-values"><span class="calc-value-box output_fd" id="fd5"></span></td></tr>
                                <tr><td>Bolt Circle Diameter (in)</td><td class="output-values"><span class="calc-value-box output_fd" id="fd6"></span></td></tr>
                                <tr><td>Gasket Seating Face Area (in<sup>2</sup>)</td><td class="output-values"><span class="calc-value-box output_fd" id="fd7"></span></td></tr>
                                <tr><td>Bolt Cross Section Area (in<sup>2</sup>)</td><td class="output-values"><span class="calc-value-box output_fd" id="fd8"></span></td></tr>
                            </table>

                            <h5 class="form-section-head"><span id="material-label"></span>&nbsp;<span id="gasketMaterial_SectionHead">Gasket Material</span></h5>
                            <table class="table table-striped" id="output_gasketMaterial">
                                <tr><td>Seating Stress (psi)</td><td class="output-values"><span class="calc-value-box output_gm" id="gm1"></span></td></tr>
                                <tr><td>Crushing Stress (psi)</td><td class="output-values"><span class="calc-value-box output_gm" id="gm2"></span></td></tr>
                            </table>
                            <h5 class="form-section-head">Thread Lubricant</h5>
                            <table class="table table-striped" id="output_threadLubricant">
                                <tr><td>Nut Factor for Calculation, K</td><td class="output-values"><span class="calc-value-box output_tl" id="tl1"></span></td></tr>
                            </table>
                            <h5 class="form-section-head">Bolt Yield Load</h5>
                            <table class="table table-striped" id="output_boltYield">
                                <colgroup>
                                    <col>
                                    <col style="text-align: right;">
                                </colgroup>
                                <tr><td>Bolt Load at Gasket Seating (psi)</td><td class="output-values"><span class="calc-value-box output_byl" id="byl1"></span></td></tr>
                                <tr><td>% of Bolt Yield at Gasket Seating</td><td class="output-values"><span class="calc-value-box output_byl" id="byl2"></span></td></tr>
                                <tr><td>Bolt Load at Gasket Crushing (psi)</td><td class="output-values"><span class="calc-value-box output_byl" id="byl3"></span></td></tr>
                                <tr><td>% of Bolt Load at Gasket Crushing</td><td class="output-values"><span class="calc-value-box output_byl" id="byl4"></span></td></tr>
                                <tr><td>Torque at Gasket Seating (ft-lbs)</td><td class="output-values"><span class="calc-value-box output_byl" id="byl5"></span></td></tr>
                                <tr><td>Torque at Gasket Crushing (ft-lbs)</td><td class="output-values"><span class="calc-value-box output_byl" id="byl6"></span></td></tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="suggested_Products">
                    <!--
                    TODO: reserve area for suggested products. Add as module
                        -->
                </div>
            </div>

        </div>

</body>
</html>
