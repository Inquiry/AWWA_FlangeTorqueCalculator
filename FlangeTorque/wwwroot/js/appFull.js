var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
function loadClassDropDown() {
    var flangeClassDropDown = document.getElementById("flangeClassDropDown");
    for (var i = 0; i < flangeClasses.length; i++) {
        var option = document.createElement("option");
        option.text = flangeClasses[i].class;
        option.value = flangeClasses[i].id.toString();
        flangeClassDropDown.add(option);
    }
    flangeClassDropDown.selectedIndex = -1;
}
/**
 * Holds the value of the currently selected class between changes
 */
var currentlySelectedPipeClass = -1;
function loadPipeSizeDropDown() {
    var newPipeClassId = parseInt(document.getElementById("flangeClassDropDown").value);
    var isDiameterListTheSame = true;
    //checks to see if a change is needed to the diameter dropdown
    if ((currentlySelectedPipeClass != FlangeClass.F && newPipeClassId === FlangeClass.F)
        || (currentlySelectedPipeClass === FlangeClass.F && newPipeClassId != FlangeClass.F)
        || newPipeClassId >= 0) {
        isDiameterListTheSame = false;
    }
    //ensures each new selection can be checked
    currentlySelectedPipeClass = newPipeClassId;
    if (isDiameterListTheSame) {
        return;
    }
    var diameterDropDown = document.getElementById("flangeDiameterDropDown");
    var activeFlangeValues = [];
    //the nominal pipe diameters only change on the F class. I don't know why
    if (newPipeClassId) {
        activeFlangeValues = classF_Values;
    }
    else {
        activeFlangeValues = classBDE_Values;
    }
    if (diameterDropDown.length > 0) {
        while (diameterDropDown.length > 0) {
            diameterDropDown.remove(0);
        }
    }
    for (var i = 0; i < activeFlangeValues.length; i++) {
        var option = document.createElement("option");
        option.text = activeFlangeValues[i].NominalPipeSize.toString();
        option.value = activeFlangeValues[i].NominalPipeSize.toString();
        diameterDropDown.add(option);
    }
    diameterDropDown.selectedIndex = -1;
}
function loadGasketDetails() {
    var gasketStyleDropDown = document.getElementById("gasketStyleDropDown");
    var gasketMaterialDropDown = document.getElementById("gasketMaterialDropDown");
    for (var i = 0; i < gasketDetails.style.length; i++) {
        var option = document.createElement("option");
        option.text = gasketDetails.style[i].type;
        option.value = gasketDetails.style[i].id.toString();
        gasketStyleDropDown.add(option);
    }
    for (var i = 0; i < gasketDetails.material.length; i++) {
        var option = document.createElement("option");
        option.text = gasketDetails.material[i].name;
        option.value = gasketDetails.material[i].id.toString();
        gasketMaterialDropDown.add(option);
    }
    gasketStyleDropDown.selectedIndex = -1;
    gasketMaterialDropDown.selectedIndex = -1;
}
/**
 * Required in order to wait for the class dropdown to load first
 */
function loadFlangeDetails() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, loadClassDropDown()];
                case 1:
                    _a.sent();
                    loadPipeSizeDropDown();
                    return [2 /*return*/];
            }
        });
    });
}
function loadInputs() {
    var inputs;
    inputs = { flangeClass: 0, nominalFlangeDiameter: 0, finishedFlangeSeatingFaceId: 0, gasketStyle: 0, gasketMaterial: 0, selectedNutFactor: 0 };
    //flange details
    inputs.flangeClass = parseInt(document.getElementById("flangeClassDropDown").value);
    inputs.nominalFlangeDiameter = parseInt(document.getElementById("flangeDiameterDropDown").value);
    var faceId = document.getElementById("flangeSeatingFace").value.trim();
    inputs.finishedFlangeSeatingFaceId = (!faceId || faceId === "") ? undefined : parseFloat(faceId);
    //gasket details
    inputs.gasketStyle = parseInt(document.getElementById("gasketStyleDropDown").value);
    inputs.gasketMaterial = parseInt(document.getElementById("gasketMaterialDropDown").value);
    //selected nut factor, K
    var k = parseFloat(document.getElementById("selectedNutValue").value.trim());
    inputs.selectedNutFactor = (!k) ? 0.0 : k;
    return inputs;
}
;
function displayFlangeTorqueCalculations(out) {
    return __awaiter(this, void 0, void 0, function () {
        var _a, i, j, field;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!(out.torqueAtGasketSeating > out.boltLoad.highTorque)) return [3 /*break*/, 1];
                    $("#targetTorque_From").text("Error!");
                    $("#targetTorque_To").text("Error!");
                    $("#boltLoadPsi_From").text("Error!");
                    $("#boltLoadPsi_To").text("Error!");
                    $("#boltLoadPercent_From").text("Error!");
                    $("#boltLoadPercent_To").text("Error!");
                    return [3 /*break*/, 4];
                case 1:
                    $("#targetTorque_From").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.minRecommendedBoltTorque) + " ft-lbs");
                    $("#targetTorque_To").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.maxRecommendedBoltTorque) + " ft-lbs");
                    return [4 /*yield*/, out.calcPsiMinMax()];
                case 2:
                    _b.sent();
                    $("#boltLoadPsi_From").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.boltLoadPsiMin) + " psi");
                    $("#boltLoadPsi_To").text(new Intl.NumberFormat("en", { maximumFractionDigits: 0 }).format(out.boltLoadPsiMax) + " psi");
                    return [4 /*yield*/, out.calcLoadPercentMinMax()];
                case 3:
                    _b.sent();
                    $("#boltLoadPercent_From").text(new Intl.NumberFormat("en", { style: "percent" }).format(out.boltLoadPercentMin));
                    $("#boltLoadPercent_To").text(new Intl.NumberFormat("en", { style: "percent" }).format(out.boltLoadPercentMax));
                    _b.label = 4;
                case 4:
                    _a = out;
                    return [4 /*yield*/, formatOutputs(out)];
                case 5:
                    _a.outputSections = _b.sent();
                    for (i = 0; i < out.outputSections.length; i++) {
                        for (j = 0; j < out.outputSections[i].values.length; j++) {
                            field = document.getElementById(out.outputSections[i].sectionId + (j + 1));
                            if (field)
                                field.innerHTML = out.outputSections[i].values[j];
                        }
                    }
                    checkForWarnings(out);
                    return [2 /*return*/];
            }
        });
    });
}
function changeMaterialLabel() {
    $("#material-label").text(function () {
        var material = document.getElementById("gasketMaterialDropDown");
        return material.options[material.selectedIndex] ? material.options[material.selectedIndex].innerHTML : "No";
    });
}
/**
 * The main. process for calculating all the flange-torque values
 */
function calculateFtOutputs() {
    return __awaiter(this, void 0, void 0, function () {
        var inputs, calculations;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, loadInputs()];
                case 1:
                    inputs = _a.sent();
                    return [4 /*yield*/, new Outputs(inputs)];
                case 2:
                    calculations = _a.sent();
                    return [2 /*return*/, calculations];
            }
        });
    });
}
function init() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    loadGasketDetails();
                    return [4 /*yield*/, loadFlangeDetails()];
                case 1:
                    _a.sent();
                    isFormFullyFilled();
                    return [2 /*return*/];
            }
        });
    });
}
function checkForWarnings(calcValues) {
    var warningLabels = document.getElementsByClassName("selection-warning");
    for (var i = 0; i < warningLabels.length; i++) {
        warningLabels[i].setAttribute("style", "display: none");
        warningLabels[i].innerHTML = "";
        switch (i) {
            case 0: {
                if (calcValues.input.finishedFlangeSeatingFaceId < calcValues.input.nominalFlangeDiameter
                    || calcValues.input.finishedFlangeSeatingFaceId > (calcValues.input.nominalFlangeDiameter + 2 + calcValues.input.nominalFlangeDiameter / 100)) {
                    warningLabels[i].setAttribute("style", "display: block");
                }
                break;
            }
            case 1: {
                if (calcValues.input.gasketStyle === GasketStyle.FullFace) {
                    if (calcValues.input.nominalFlangeDiameter > 24 || calcValues.flange.class === FlangeClass.F) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="fullFace_warning" class="warning-message">Full Face Gaskets are not allowed in AWWA C207 for Class F or nom. ' +
                            'diameters over 24”, bolt yield can be exceeded before obtaining gasket seating stress</p>';
                        //document.getElementById("fullFace_warning").style.display = "block";
                    }
                }
                if (calcValues.material.Name.toLowerCase() == "rubber") {
                    if (calcValues.flange.class === FlangeClass.E || calcValues.flange.class === FlangeClass.F) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="rubber_warning" class="warning-message">AWWA C207 does not allow Elastomeric (Rubber) Ring Gaskets for Class F, ' +
                            'or Class E; over 24 in., over 175 psi up to 12 in., over 150 psi greater than 12 in. up to 24 in. </p>';
                        //document.getElementById("rubber_warning").style.display = "block";
                    }
                }
                break;
            }
            case 2: {
                if (calcValues.torqueAtGasketSeating > calcValues.boltLoad.highTorque) {
                    warningLabels[i].setAttribute("style", "display: block");
                    warningLabels[i].innerHTML += '<p id="seatingStress_warning" class="warning-message" > Gasket seating stress develops a ' +
                        'required load in excess of the allowable bolt load, change the gasket</p>';
                }
                if (calcValues.torqueAtGasketSeating < calcValues.boltLoad.highTorque) {
                    if (calcValues.maxRecommendedBoltTorque > calcValues.boltLoad.highTorque
                        && calcValues.maxRecommendedBoltTorque <= calcValues.boltLoad.excessiveTorque) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="overstress_warning" class="warning-message">Select a value from the middle of ' +
                            'recommended target torque range to reduce possible bolt overstress.</p>';
                    }
                    if (calcValues.minRecommendedBoltTorque < calcValues.boltLoad.lowTorque) {
                        warningLabels[i].setAttribute("style", "display: block");
                        warningLabels[i].innerHTML += '<p id="crushing_warning" class="warning-message">Due to potential gasket crushing, torque values are below recommended bolt loading ' +
                            'for a stable joint.<br />Selection of a different gasket material may be necessary.</p>';
                    }
                }
                break;
            }
        }
    }
}
//the logic for recalculating on input change
function recalculateValues() {
    return __awaiter(this, void 0, void 0, function () {
        var newCalculation;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!isFormFullyFilled()) return [3 /*break*/, 2];
                    return [4 /*yield*/, calculateFtOutputs()];
                case 1:
                    newCalculation = _a.sent();
                    displayFlangeTorqueCalculations(newCalculation);
                    return [3 /*break*/, 3];
                case 2:
                    window.alert("One or more required fields still needs to be filled");
                    _a.label = 3;
                case 3: return [2 /*return*/];
            }
        });
    });
}
function isFormFullyFilled() {
    var isFilled = true;
    //ensures all fields are populated properly before attempting a calculation
    var requiredFields = document.getElementsByClassName("required");
    for (var i = 0; i < requiredFields.length; i++) {
        if (requiredFields[i].hasAttribute("selectedIndex")) {
            if (requiredFields[i].selectedIndex === -1) {
                isFilled = false;
                requiredFields[i].className += " alert-danger";
            }
            else {
                requiredFields[i].className = "required calc-inputs";
            }
        }
        else {
            if (requiredFields[i].value.trim() === "") {
                isFilled = false;
                requiredFields[i].className += " alert-danger";
            }
            else {
                requiredFields[i].className = "required calc-inputs";
            }
        }
    }
    return isFilled;
}
function loadLinks() {
    $(".m11-17").attr("href", domain + storeProductBase + m11_id);
    $(".c207-13").attr("href", domain + storeProductBase + c207_id);
    $(".c604-17").attr("href", domain + storeProductBase + c604_id);
    $("#contactUsButton").attr("href", domain + "/about-us/contact-us.aspx");
    $("#backToCommunityButton").attr("href", domain + "/resources-tools/water-knowledge.aspx");
    $("#instructionButton").attr("href", "/FTCResources/UserInstructions.docx");
    $("#awwaLogo").attr("href", domain);
}
//binds enter key to calculate values
$(function () {
    $(document).on("keypress", function (e) {
        if (e.which == 13) {
            recalculateValues();
        }
    });
});
/**
 * Exactly what it says on the tin: Clears the calculator's inputs for a new calculation
 */
function clearInputs() {
    var calcInputs = document.getElementsByClassName("calc-inputs");
    for (var i = 0; i < calcInputs.length; i++) {
        if (calcInputs[i].hasAttribute("selectedIndex")) {
            calcInputs[i].selectedIndex = -1;
        }
        else {
            calcInputs[i].value = "";
        }
    }
    var diameterDropDown = document.getElementById("flangeDiameterDropDown");
    if (diameterDropDown.length > 0) {
        while (diameterDropDown.length > 0) {
            diameterDropDown.remove(0);
        }
    }
    $("#targetTorque_From").text("");
    $("#targetTorque_To").text("");
    $("#boltLoadPsi_From").text("");
    $("#boltLoadPsi_To").text("");
    $("#boltLoadPercent_From").text("");
    $("#boltLoadPercent_To").text("");
    $(".calc-value-box").text("");
    $(".selection-warning").css("display", "none");
    isFormFullyFilled();
}
window.onload = function () {
    init();
    changeMaterialLabel();
    //fillRotator(); //not needed for DNN
    loadLinks();
};
//# sourceMappingURL=app.js.map
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var BoltLoad = (function () {
    function BoltLoad(staticValues, kValue) {
        //all the torque values rely on PSI being calculated first
        this.calculateTorqueRanges(staticValues, kValue);
    }
    BoltLoad.prototype.calculatePsi = function (staticValues) {
        return __awaiter(this, void 0, void 0, function () {
            var testLow;
            return __generator(this, function (_a) {
                testLow = this.BoltLoadPsi(0.2, staticValues.minYieldStress);
                this.lowPsi = (testLow < 25000) ? 25000 : testLow;
                this.targetPsi = this.BoltLoadPsi(0.5, staticValues.minYieldStress);
                this.highPsi = this.BoltLoadPsi(0.7, staticValues.minYieldStress);
                this.excessivePsi = this.BoltLoadPsi(0.9, staticValues.minYieldStress);
                this.yieldPsi = this.BoltLoadPsi(1, staticValues.minYieldStress);
                return [2 /*return*/];
            });
        });
    };
    BoltLoad.prototype.calculateTorqueRanges = function (staticValues, kValue) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.calculatePsi(staticValues)];
                    case 1:
                        _a.sent();
                        this.lowTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.lowPsi);
                        this.targetTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.targetPsi);
                        this.highTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.highPsi);
                        this.excessiveTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.excessivePsi);
                        this.yieldTorque = this.BoltLoadTorque(kValue, staticValues.boltSize, this.yieldPsi);
                        return [2 /*return*/];
                }
            });
        });
    };
    BoltLoad.prototype.BoltLoadPsi = function (percentage, minYieldStress) {
        if (percentage > 1 || percentage < 0) {
            throw "Percentage not within expected range. Must be between 0 and 1";
        }
        return percentage * minYieldStress;
    };
    BoltLoad.prototype.BoltLoadTorque = function (kValue, boltSize, psi) {
        if (!psi) {
            throw "PSI has not be calculated yet"; //TODO: remove this before deployment. Callback may be needed
        }
        return kValue * boltSize * psi / 12;
    };
    return BoltLoad;
}());
//# sourceMappingURL=BoltLoad.js.map
var domain = "";
var storeProductBase = "/store/productdetail.aspx?productid=";
var m11_id = 62572078;
var c207_id = 35009101;
var c604_id = 66610737;
var GasketStyle;
(function (GasketStyle) {
    GasketStyle[GasketStyle["Ring"] = 0] = "Ring";
    GasketStyle[GasketStyle["FullFace"] = 1] = "FullFace";
})(GasketStyle || (GasketStyle = {}));
var MaterialName;
(function (MaterialName) {
    MaterialName[MaterialName["Rubber"] = 0] = "Rubber";
    MaterialName[MaterialName["CompositeFiber"] = 1] = "CompositeFiber";
})(MaterialName || (MaterialName = {}));
var FlangeClass;
(function (FlangeClass) {
    FlangeClass[FlangeClass["B"] = 0] = "B";
    FlangeClass[FlangeClass["D"] = 1] = "D";
    FlangeClass[FlangeClass["E"] = 2] = "E";
    FlangeClass[FlangeClass["F"] = 3] = "F";
})(FlangeClass || (FlangeClass = {}));
//# sourceMappingURL=Constants.js.map
var Flange = (function () {
    function Flange(input) {
        this.nominalPipeSize = input.nominalFlangeDiameter;
        this.class = input.flangeClass;
        if (input.finishedFlangeSeatingFaceId)
            this.seatingFaceId = input.finishedFlangeSeatingFaceId;
    }
    return Flange;
}());
//# sourceMappingURL=Flange.js.map
//# sourceMappingURL=interfaces.js.map
//Contains all the variables and calculations for a gasket material type
var Material = (function () {
    function Material(materialTypeId) {
        //Get the seating and crushing strength based on the material type        
        for (var i = 0; i < gasketDetails.material.length; i++) {
            if (gasketDetails.material[i].id === materialTypeId) {
                this.SeatingStress = gasketDetails.material[i].seatingStress;
                this.CrushingStress = gasketDetails.material[i].crushingStress;
                this.Name = MaterialName[materialTypeId];
                break;
            }
        }
    }
    return Material;
}());
//# sourceMappingURL=Material.js.map
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Outputs = (function () {
    function Outputs(calcInput) {
        this.calculateOutputs(calcInput);
    }
    /**
     * Calculates all the output values before being assigned to the sections
     * @param calcInput
     * @param loadOutputs
     */
    Outputs.prototype.calculateOutputs = function (calcInput) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.input = calcInput;
                        this.material = new Material(calcInput.gasketMaterial);
                        this.flange = new Flange(calcInput);
                        this.calculations = new CalculatedValues(this.flange, this.material, calcInput);
                        this.boltLoad = new BoltLoad(this.calculations, calcInput.selectedNutFactor);
                        if (this.material && this.flange && this.calculations && this.boltLoad) {
                            this.loadAtGasketSeating = this.calculatePsi(this.calculations.seatingStress, this.calculations.gasketSeatingFaceArea, this.calculations.numberOfBolts, this.calculations.boltCrossSection);
                            this.loadAtGasketCrushing = this.calculatePsi(this.calculations.crushingStress, this.calculations.gasketSeatingFaceArea, this.calculations.numberOfBolts, this.calculations.boltCrossSection);
                        }
                        else {
                            throw "still not calculating";
                        }
                        return [4 /*yield*/, this.secondCalc()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Outputs.prototype.secondCalc = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ranges;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.torqueAtGasketSeating = this.calculateTorque(this.input.selectedNutFactor, this.calculations.boltSize, this.loadAtGasketSeating);
                        this.torqueAtGasketCrushing = this.calculateTorque(this.input.selectedNutFactor, this.calculations.boltSize, this.loadAtGasketCrushing);
                        this.percentAtSeating = this.loadAtGasketSeating / this.calculations.minYieldStress;
                        this.percentAtCrushing = this.loadAtGasketCrushing / this.calculations.minYieldStress;
                        return [4 /*yield*/, this.getRecommendedTorque()];
                    case 1:
                        ranges = _a.sent();
                        this.maxRecommendedBoltTorque = ranges.max;
                        this.minRecommendedBoltTorque = ranges.min;
                        return [2 /*return*/];
                }
            });
        });
    };
    Outputs.prototype.calcPsiMinMax = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.boltLoadPsiMin = this.getBoltLoadPsiAtTorque(this.minRecommendedBoltTorque, this.input.selectedNutFactor, this.calculations.boltSize);
                this.boltLoadPsiMax = this.getBoltLoadPsiAtTorque(this.maxRecommendedBoltTorque, this.input.selectedNutFactor, this.calculations.boltSize);
                return [2 /*return*/];
            });
        });
    };
    Outputs.prototype.calcLoadPercentMinMax = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.boltLoadPercentMin = this.boltLoadPsiMin / this.calculations.minYieldStress;
                this.boltLoadPercentMax = this.boltLoadPsiMax / this.calculations.minYieldStress;
                return [2 /*return*/];
            });
        });
    };
    Outputs.prototype.calculatePsi = function (stress, gasketSeatingFaceArea, numberOfBolts, boltCrossSection) {
        var psi = gasketSeatingFaceArea * stress / numberOfBolts / boltCrossSection;
        return psi;
    };
    Outputs.prototype.calculateTorque = function (nutFactor, boltDiameter, boltLoading) {
        var torque = nutFactor * boltDiameter * boltLoading / 12.0;
        return torque;
    };
    Outputs.prototype.getRecommendedTorque = function () {
        return __awaiter(this, void 0, void 0, function () {
            var range, minTorque, maximumMinTorque;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        range = { max: 0, min: 0 };
                        if (!!this.boltLoad.highTorque) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.boltLoad];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        range.max = Math.max(this.torqueAtGasketSeating * 1.15, Math.min(this.torqueAtGasketCrushing * 0.9, this.input.selectedNutFactor * this.calculations.boltSize * 1.1 * this.boltLoad.targetPsi / 12.0));
                        range.max = Math.round(range.max / 5.0) * 5.0; //range.max % 5.0 >= 2.5 ? Math.round(range.max / 5.0) * 5.0 + 5.0 : Math.round(range.max / 5.0) * 5.0;
                        minTorque = this.input.selectedNutFactor * this.calculations.boltSize * 0.9 * this.boltLoad.targetPsi / 12.0;
                        maximumMinTorque = this.torqueAtGasketSeating * 1.1;
                        if (Math.max(maximumMinTorque, minTorque) > 0.8 * range.max) {
                            range.min = 0.8 * range.max;
                        }
                        else {
                            range.min = Math.max(maximumMinTorque, minTorque);
                            range.min = Math.round(range.min / 5.0) * 5.0; //range.min % 5.0 >= 2.5 ? Math.round(range.min / 5.0) * 5.0 + 5.0 : Math.round(range.min / 5.0) * 5.0;
                        }
                        return [2 /*return*/, range];
                }
            });
        });
    };
    Outputs.prototype.getBoltLoadPsiAtTorque = function (recommendedTorque, selectedNutFactor, boltSize) {
        return recommendedTorque * 12.0 / selectedNutFactor / boltSize;
    };
    return Outputs;
}());
function formatOutputs(calcOutputs) {
    return __awaiter(this, void 0, void 0, function () {
        var flangeDetails, gasketMaterial, threadLubricant, boltYieldLoad;
        return __generator(this, function (_a) {
            flangeDetails = new OutputSection;
            flangeDetails.sectionId = "fd";
            flangeDetails.values = [
                calcOutputs.calculations.numberOfBolts.toString(),
                calcOutputs.calculations.boltSize.toFixed(3) + " in.",
                new Intl.NumberFormat("en", { style: "decimal" }).format(calcOutputs.calculations.minYieldStress) + " psi",
                calcOutputs.calculations.flangeID.toFixed(2) + " in.",
                calcOutputs.calculations.flangeOD + " in.",
                calcOutputs.calculations.boltCircle.toFixed(2) + " in.",
                calcOutputs.calculations.gasketSeatingFaceArea.toFixed(1) + " in.<sup>2</sup>",
                calcOutputs.calculations.boltCrossSection.toFixed(3) + " in.<sup>2</sup>"
            ];
            gasketMaterial = new OutputSection;
            gasketMaterial.sectionId = "gm";
            gasketMaterial.values = [
                new Intl.NumberFormat("en", { style: "decimal" }).format(calcOutputs.material.SeatingStress) + " psi",
                new Intl.NumberFormat("en", { style: "decimal" }).format(calcOutputs.material.CrushingStress) + " psi"
            ];
            threadLubricant = new OutputSection;
            threadLubricant.sectionId = "tl";
            threadLubricant.values = [
                calcOutputs.input.selectedNutFactor.toString()
            ];
            boltYieldLoad = new OutputSection;
            boltYieldLoad.sectionId = "byl";
            boltYieldLoad.values = [
                new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.loadAtGasketSeating) + " psi",
                new Intl.NumberFormat("en", { style: "percent" }).format(calcOutputs.percentAtSeating),
                new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.loadAtGasketCrushing) + " psi",
                new Intl.NumberFormat("en", { style: "percent" }).format(calcOutputs.percentAtCrushing),
                new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.torqueAtGasketSeating) + " ft-lbs",
                new Intl.NumberFormat("en", { style: "decimal", maximumFractionDigits: 0 }).format(calcOutputs.torqueAtGasketCrushing) + " ft-lbs",
            ];
            return [2 /*return*/, [flangeDetails, gasketMaterial, threadLubricant, boltYieldLoad]];
        });
    });
}
var OutputSection = (function () {
    function OutputSection() {
    }
    return OutputSection;
}());
//# sourceMappingURL=Outputs.js.map
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
function fillRotator() {
    return __awaiter(this, void 0, void 0, function () {
        var linkedProductDiv, i, product;
        return __generator(this, function (_a) {
            linkedProductDiv = $(".linked-products")[0];
            //for (let i = 0; i < activeProducts.length; i++) {
            //    linkedProductDiv.innerHTML += `<div class="linked-product">` +
            //        `<a href="${activeProducts[i].url}">` +
            //        `<img src="images/relatedProducts/${activeProducts[i].image}" alt="${activeProducts[i].title}">` +
            //        `<p style="color:inherit;">${activeProducts[i].title}</p>` +
            //        `</a>` +
            //        `<p>LIST PRICE: <span>${activeProducts[i].listPrice}</span></p>` +
            //        `<p>MEMBER PRICE: <span class="member-price">${activeProducts[i].memberPrice}</span></p>` +
            //        `</div>`;
            //}
            for (i = 0; i < activeProductIds.length; i++) {
                product = new Product(activeProductIds[i]);
                if (!product.title)
                    continue; //no need to populate something if no info is grabbed
                linkedProductDiv.innerHTML += "<div class=\"linked-product\">" +
                    ("<a href=\"" + product.url + "\">") +
                    ("<img src=\"images/relatedProducts/" + product.image + "\" alt=\"" + product.title + "\">") +
                    ("<p style=\"color:inherit;\">" + product.title + "</p>") +
                    "</a>" +
                    ("<p>LIST PRICE: <span>" + product.listPrice + "</span></p>") +
                    ("<p>MEMBER PRICE: <span class=\"member-price\">" + product.memberPrice + "</span></p>") +
                    "</div>";
            }
            return [2 /*return*/];
        });
    });
}
/**
 * Scrapes an url for product metadata in order to populate itself
 */
var Product = (function () {
    function Product(productId) {
        this.url = domain + storeProductBase + productId;
        //using jQuery because that was the first thing I could find
        $.get(this.url, function (data) {
            this.title = $(data).find("meta[property=og:title]").attr("content");
            this.image = $(data).find("meta[property=og:image]").attr("content");
            this.listPrice = $(data).find("meta[property=og:price]").attr("content");
            this.memberPrice = $(data).find("span[id*=ProductINVBuyBoxControl_PriceDisplayControl1_MemberPrice_PriceValueLabel").val();
        });
    }
    Object.defineProperty(Product.prototype, "listPrice", {
        get: function () {
            return new Intl.NumberFormat("en", { style: "currency", currency: "USD" }).format(this._listPrice);
        },
        set: function (amount) {
            amount = amount.replace(/\D/g, "");
            this._listPrice = parseFloat(amount) ? parseFloat(amount) : 0.0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "memberPrice", {
        get: function () {
            return new Intl.NumberFormat("en", { style: "currency", currency: "USD" }).format(this._memberPrice);
        },
        set: function (amount) {
            amount = amount.replace(/^\D/, "");
            this._memberPrice = parseFloat(amount) ? parseFloat(amount) : 0.0;
        },
        enumerable: true,
        configurable: true
    });
    return Product;
}());
//# sourceMappingURL=Rotator.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///Pulls and stores the values from the resource files
var StaticValues = (function () {
    function StaticValues(flange, material) {
        var resourceFile;
        this.seatingStress = material.SeatingStress;
        this.crushingStress = material.CrushingStress;
        if (flange.class === FlangeClass.F) {
            for (var i = 0; i < classF_Values.length; i++) {
                if (flange.nominalPipeSize === classF_Values[i].NominalPipeSize) {
                    resourceFile = classF_Values[i];
                    break;
                }
            }
        }
        else {
            for (var i = 0; i < classBDE_Values.length; i++) {
                if (flange.nominalPipeSize === classBDE_Values[i].NominalPipeSize) {
                    resourceFile = classBDE_Values[i];
                    break;
                }
            }
        }
        if (resourceFile) {
            this.nominalPipeSize = resourceFile.NominalPipeSize;
            this.numberOfBolts = resourceFile.NumberofBolts;
            this.boltSizeDiameter = resourceFile.BoltSizeDiameter;
            this.boltSize = resourceFile.BoltSize;
            this.threadsPerInch = resourceFile.ThreadsperInch;
            this.heavyHexNutSize = resourceFile.HeavyHexNutSize;
            this.minYieldStress = resourceFile.MinYieldStress;
            this.flangeID = flange.seatingFaceId ? flange.seatingFaceId : resourceFile.FlangeID;
            this.flangeOD = resourceFile.FlangeOD;
            this.boltCircle = resourceFile.BoltCircle;
        }
    }
    return StaticValues;
}());
var CalculatedValues = (function (_super) {
    __extends(CalculatedValues, _super);
    function CalculatedValues(flange, material, input) {
        var _this = _super.call(this, flange, material) || this;
        _this.nominalBoltArea = Math.pow((_this.boltSize / 2), 2) * Math.PI;
        _this.boltCircleID = _this.boltCircle - _this.boltSize - 0.125;
        _this.ringArea = Math.pow((_this.boltCircleID / 2), 2) * Math.PI - Math.pow((_this.flangeID / 2), 2) * Math.PI;
        _this.fullArea = Math.pow((_this.flangeOD / 2), 2) * Math.PI - Math.pow((_this.flangeID / 2), 2) * Math.PI;
        _this.ringMin = _this.seatingStress * _this.ringArea / _this.numberOfBolts / _this.boltSize;
        _this.ringMax = _this.crushingStress * _this.ringArea / _this.numberOfBolts / _this.nominalBoltArea;
        _this.ringMinPercent = _this.ringMin / _this.ringMax;
        _this.ringMaxPercent = _this.ringMax / _this.minYieldStress;
        _this.fullMin = _this.seatingStress * _this.fullArea / _this.numberOfBolts / _this.nominalBoltArea;
        _this.fullMax = _this.crushingStress * _this.fullArea / _this.numberOfBolts / _this.nominalBoltArea;
        _this.fullMinPercent = _this.fullMin / _this.fullMax;
        _this.fullMaxPercent = _this.fullMax / _this.minYieldStress;
        _this.boltTension_50percent = _this.minYieldStress * (0.5) * _this.nominalBoltArea;
        if (input.gasketStyle === GasketStyle.Ring) {
            _this.gasketSeatingFaceArea = (Math.pow((_this.boltCircle - _this.boltSize - 0.125), 2) - Math.pow(_this.flangeID, 2)) * Math.PI / 4;
        }
        else {
            _this.gasketSeatingFaceArea = ((Math.pow(_this.flangeOD, 2) - Math.pow(_this.flangeID, 2)) * Math.PI / 4 - _this.numberOfBolts * (Math.pow((0.125 + _this.boltSize), 2) * Math.PI / 4));
        }
        _this.boltCrossSection = Math.pow((_this.boltSize / 2), 2) * Math.PI;
        return _this;
    }
    //Calculate the Torque for the bolt yield percent and K factor
    CalculatedValues.prototype.getGasketPercent = function (percentBoltYield, selectedNutFactor) {
        if (percentBoltYield > 1) {
            var err = "Percent bolt yield cannot be greater than 1";
            throw err;
        }
        var gasketPercent = this.minYieldStress * this.nominalBoltArea / 12 * percentBoltYield * selectedNutFactor;
        if (!gasketPercent) {
            throw "There was an error in calculating gasketPercent. Not all values present";
        }
        //round to the nearest 5%
        gasketPercent = gasketPercent * 100;
        gasketPercent = gasketPercent % 5.0 >= 2.5 ? Math.round(gasketPercent / 5.0) * 5.0 + 5.0 : Math.round(gasketPercent / 5.0) * 5.0;
        gasketPercent = gasketPercent / 100;
        return gasketPercent;
    };
    return CalculatedValues;
}(StaticValues));
//# sourceMappingURL=Values.js.map
var flangeClasses = [
    { "class": "B", "id": 0 },
    { "class": "D", "id": 1 },
    { "class": "E", "id": 2 },
    { "class": "F", "id": 3 }
];
var gasketDetails = {
    "style": [
        { "type": "Ring", "id": 0 },
        { "type": "Full Face", "id": 1 }
    ],
    "material": [
        {
            "id": 0,
            "name": "Rubber (Elastomeric)",
            "seatingStress": 500,
            "crushingStress": 1500
        },
        {
            "id": 1,
            "name": "CFG/PTFE",
            "seatingStress": 4800,
            "crushingStress": 15000
        }
    ]
};
//# sourceMappingURL=BoltTorqueRanges.js.map
var classBDE_Values = [
    {
        "NominalPipeSize": 4,
        "NumberofBolts": 8,
        "BoltSizeDiameter": "5/8",
        "BoltSize": 0.625,
        "ThreadsperInch": 11,
        "HeavyHexNutSize": "1-1/16",
        "MinYieldStress": 105000,
        "FlangeID": 4.57,
        "FlangeOD": 9.00,
        "BoltCircle": 7.50
    },
    {
        "NominalPipeSize": 5,
        "NumberofBolts": 8,
        "BoltSizeDiameter": "3/4",
        "BoltSize": 0.750,
        "ThreadsperInch": 10,
        "HeavyHexNutSize": "1-1/4",
        "MinYieldStress": 105000,
        "FlangeID": 5.66,
        "FlangeOD": 10.00,
        "BoltCircle": 8.50
    },
    {
        "NominalPipeSize": 6,
        "NumberofBolts": 8,
        "BoltSizeDiameter": "3/4",
        "BoltSize": 0.750,
        "ThreadsperInch": 10,
        "HeavyHexNutSize": "1-1/4",
        "MinYieldStress": 105000,
        "FlangeID": 6.72,
        "FlangeOD": 11.00,
        "BoltCircle": 9.50
    },
    {
        "NominalPipeSize": 8,
        "NumberofBolts": 8,
        "BoltSizeDiameter": "3/4",
        "BoltSize": 0.750,
        "ThreadsperInch": 10,
        "HeavyHexNutSize": "1-1/4",
        "MinYieldStress": 105000,
        "FlangeID": 8.72,
        "FlangeOD": 13.50,
        "BoltCircle": 11.75
    },
    {
        "NominalPipeSize": 10,
        "NumberofBolts": 12,
        "BoltSizeDiameter": "7/8",
        "BoltSize": 0.875,
        "ThreadsperInch": 9,
        "HeavyHexNutSize": "1-7/16",
        "MinYieldStress": 105000,
        "FlangeID": 10.88,
        "FlangeOD": 16.00,
        "BoltCircle": 14.25
    },
    {
        "NominalPipeSize": 12,
        "NumberofBolts": 12,
        "BoltSizeDiameter": "7/8",
        "BoltSize": 0.875,
        "ThreadsperInch": 9,
        "HeavyHexNutSize": "1-7/16",
        "MinYieldStress": 105000,
        "FlangeID": 12.88,
        "FlangeOD": 19.00,
        "BoltCircle": 17.00
    },
    {
        "NominalPipeSize": 14,
        "NumberofBolts": 12,
        "BoltSizeDiameter": "1",
        "BoltSize": 1.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "1-5/8",
        "MinYieldStress": 105000,
        "FlangeID": 14.19,
        "FlangeOD": 21.00,
        "BoltCircle": 18.75
    },
    {
        "NominalPipeSize": 16,
        "NumberofBolts": 16,
        "BoltSizeDiameter": "1",
        "BoltSize": 1.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "1-5/8",
        "MinYieldStress": 105000,
        "FlangeID": 16.19,
        "FlangeOD": 23.50,
        "BoltCircle": 21.25
    },
    {
        "NominalPipeSize": 18,
        "NumberofBolts": 16,
        "BoltSizeDiameter": "1-1/8",
        "BoltSize": 1.125,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "1-13/16",
        "MinYieldStress": 105000,
        "FlangeID": 18.19,
        "FlangeOD": 25.00,
        "BoltCircle": 22.75
    },
    {
        "NominalPipeSize": 20,
        "NumberofBolts": 20,
        "BoltSizeDiameter": "1-1/8",
        "BoltSize": 1.125,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "1-13/16",
        "MinYieldStress": 105000,
        "FlangeID": 20.19,
        "FlangeOD": 27.50,
        "BoltCircle": 25.00
    },
    {
        "NominalPipeSize": 22,
        "NumberofBolts": 20,
        "BoltSizeDiameter": "1-1/4",
        "BoltSize": 1.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2",
        "MinYieldStress": 105000,
        "FlangeID": 22.19,
        "FlangeOD": 29.50,
        "BoltCircle": 27.25
    },
    {
        "NominalPipeSize": 24,
        "NumberofBolts": 20,
        "BoltSizeDiameter": "1-1/4",
        "BoltSize": 1.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2",
        "MinYieldStress": 105000,
        "FlangeID": 24.19,
        "FlangeOD": 32.00,
        "BoltCircle": 29.50
    },
    {
        "NominalPipeSize": 26,
        "NumberofBolts": 24,
        "BoltSizeDiameter": "1-1/4",
        "BoltSize": 1.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2",
        "MinYieldStress": 105000,
        "FlangeID": 28.00,
        "FlangeOD": 34.25,
        "BoltCircle": 31.75
    },
    {
        "NominalPipeSize": 28,
        "NumberofBolts": 28,
        "BoltSizeDiameter": "1-1/4",
        "BoltSize": 1.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2",
        "MinYieldStress": 105000,
        "FlangeID": 30.00,
        "FlangeOD": 36.50,
        "BoltCircle": 34.00
    },
    {
        "NominalPipeSize": 30,
        "NumberofBolts": 28,
        "BoltSizeDiameter": "1-1/4",
        "BoltSize": 1.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2",
        "MinYieldStress": 105000,
        "FlangeID": 32.00,
        "FlangeOD": 38.75,
        "BoltCircle": 36.00
    },
    {
        "NominalPipeSize": 32,
        "NumberofBolts": 28,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 34.00,
        "FlangeOD": 41.75,
        "BoltCircle": 38.50
    },
    {
        "NominalPipeSize": 34,
        "NumberofBolts": 32,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 36.00,
        "FlangeOD": 43.75,
        "BoltCircle": 40.50
    },
    {
        "NominalPipeSize": 36,
        "NumberofBolts": 32,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 38.00,
        "FlangeOD": 46.00,
        "BoltCircle": 42.75
    },
    {
        "NominalPipeSize": 38,
        "NumberofBolts": 32,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 40.00,
        "FlangeOD": 48.75,
        "BoltCircle": 45.25
    },
    {
        "NominalPipeSize": 40,
        "NumberofBolts": 36,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 42.00,
        "FlangeOD": 50.75,
        "BoltCircle": 47.25
    },
    {
        "NominalPipeSize": 42,
        "NumberofBolts": 36,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 44.00,
        "FlangeOD": 53.00,
        "BoltCircle": 49.50
    },
    {
        "NominalPipeSize": 44,
        "NumberofBolts": 40,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 46.00,
        "FlangeOD": 55.25,
        "BoltCircle": 51.75
    },
    {
        "NominalPipeSize": 46,
        "NumberofBolts": 40,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 48.00,
        "FlangeOD": 57.25,
        "BoltCircle": 53.75
    },
    {
        "NominalPipeSize": 48,
        "NumberofBolts": 44,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 50.00,
        "FlangeOD": 59.50,
        "BoltCircle": 56.00
    },
    {
        "NominalPipeSize": 50,
        "NumberofBolts": 44,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 52.00,
        "FlangeOD": 61.75,
        "BoltCircle": 58.25
    },
    {
        "NominalPipeSize": 52,
        "NumberofBolts": 44,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 54.00,
        "FlangeOD": 64.00,
        "BoltCircle": 60.50
    },
    {
        "NominalPipeSize": 54,
        "NumberofBolts": 44,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 56.00,
        "FlangeOD": 66.25,
        "BoltCircle": 62.75
    },
    {
        "NominalPipeSize": 60,
        "NumberofBolts": 52,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 62.00,
        "FlangeOD": 73.00,
        "BoltCircle": 69.25
    },
    {
        "NominalPipeSize": 66,
        "NumberofBolts": 52,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 68.00,
        "FlangeOD": 80.00,
        "BoltCircle": 76.00
    },
    {
        "NominalPipeSize": 72,
        "NumberofBolts": 60,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 74.00,
        "FlangeOD": 86.50,
        "BoltCircle": 82.50
    },
    {
        "NominalPipeSize": 78,
        "NumberofBolts": 64,
        "BoltSizeDiameter": "2",
        "BoltSize": 2.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/8",
        "MinYieldStress": 105000,
        "FlangeID": 80.00,
        "FlangeOD": 93.00,
        "BoltCircle": 89.00
    },
    {
        "NominalPipeSize": 84,
        "NumberofBolts": 64,
        "BoltSizeDiameter": "2",
        "BoltSize": 2.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/8",
        "MinYieldStress": 105000,
        "FlangeID": 86.00,
        "FlangeOD": 99.75,
        "BoltCircle": 95.50
    },
    {
        "NominalPipeSize": 90,
        "NumberofBolts": 68,
        "BoltSizeDiameter": "2-1/4",
        "BoltSize": 2.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/2",
        "MinYieldStress": 105000,
        "FlangeID": 92.00,
        "FlangeOD": 106.50,
        "BoltCircle": 102.00
    },
    {
        "NominalPipeSize": 96,
        "NumberofBolts": 68,
        "BoltSizeDiameter": "2-1/4",
        "BoltSize": 2.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/2",
        "MinYieldStress": 105000,
        "FlangeID": 98.00,
        "FlangeOD": 113.25,
        "BoltCircle": 108.50
    },
    {
        "NominalPipeSize": 102,
        "NumberofBolts": 72,
        "BoltSizeDiameter": "2-1/2",
        "BoltSize": 2.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-7/8",
        "MinYieldStress": 105000,
        "FlangeID": 104.00,
        "FlangeOD": 120.00,
        "BoltCircle": 114.50
    },
    {
        "NominalPipeSize": 108,
        "NumberofBolts": 72,
        "BoltSizeDiameter": "2-1/2",
        "BoltSize": 2.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-7/8",
        "MinYieldStress": 105000,
        "FlangeID": 110.00,
        "FlangeOD": 126.75,
        "BoltCircle": 120.75
    },
    {
        "NominalPipeSize": 114,
        "NumberofBolts": 76,
        "BoltSizeDiameter": "2-3/4",
        "BoltSize": 2.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "4-1/4",
        "MinYieldStress": 95000,
        "FlangeID": 116.00,
        "FlangeOD": 133.50,
        "BoltCircle": 126.75
    },
    {
        "NominalPipeSize": 120,
        "NumberofBolts": 76,
        "BoltSizeDiameter": "2-3/4",
        "BoltSize": 2.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "4-1/4",
        "MinYieldStress": 95000,
        "FlangeID": 122.00,
        "FlangeOD": 140.25,
        "BoltCircle": 132.75
    },
    {
        "NominalPipeSize": 126,
        "NumberofBolts": 80,
        "BoltSizeDiameter": "3",
        "BoltSize": 3.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "4-5/8",
        "MinYieldStress": 95000,
        "FlangeID": 128.00,
        "FlangeOD": 147.00,
        "BoltCircle": 139.25
    },
    {
        "NominalPipeSize": 132,
        "NumberofBolts": 80,
        "BoltSizeDiameter": "3",
        "BoltSize": 3.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "4-5/8",
        "MinYieldStress": 95000,
        "FlangeID": 134.00,
        "FlangeOD": 153.75,
        "BoltCircle": 145.75
    },
    {
        "NominalPipeSize": 144,
        "NumberofBolts": 84,
        "BoltSizeDiameter": "3-1/4",
        "BoltSize": 3.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "5",
        "MinYieldStress": 95000,
        "FlangeID": 146.00,
        "FlangeOD": 167.25,
        "BoltCircle": 158.25
    }
];
//# sourceMappingURL=ClassBDE_Values.js.map
var classF_Values = [
    {
        "NominalPipeSize": 4,
        "NumberofBolts": 8,
        "BoltSizeDiameter": "3/4",
        "BoltSize": 0.750,
        "ThreadsperInch": 10,
        "HeavyHexNutSize": "1-1/4",
        "MinYieldStress": 105000,
        "FlangeID": 4.57,
        "FlangeOD": 10.00,
        "BoltCircle": 7.88
    },
    {
        "NominalPipeSize": 5,
        "NumberofBolts": 8,
        "BoltSizeDiameter": "3/4",
        "BoltSize": 0.750,
        "ThreadsperInch": 10,
        "HeavyHexNutSize": "1-1/4",
        "MinYieldStress": 105000,
        "FlangeID": 5.66,
        "FlangeOD": 11.00,
        "BoltCircle": 9.25
    },
    {
        "NominalPipeSize": 6,
        "NumberofBolts": 12,
        "BoltSizeDiameter": "3/4",
        "BoltSize": 0.750,
        "ThreadsperInch": 10,
        "HeavyHexNutSize": "1-1/4",
        "MinYieldStress": 105000,
        "FlangeID": 6.72,
        "FlangeOD": 12.50,
        "BoltCircle": 10.62
    },
    {
        "NominalPipeSize": 8,
        "NumberofBolts": 12,
        "BoltSizeDiameter": "7/8",
        "BoltSize": 0.875,
        "ThreadsperInch": 9,
        "HeavyHexNutSize": "1-7/16",
        "MinYieldStress": 105000,
        "FlangeID": 8.72,
        "FlangeOD": 15.00,
        "BoltCircle": 13.00
    },
    {
        "NominalPipeSize": 10,
        "NumberofBolts": 16,
        "BoltSizeDiameter": "1",
        "BoltSize": 1.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "1-5/8",
        "MinYieldStress": 105000,
        "FlangeID": 10.88,
        "FlangeOD": 17.50,
        "BoltCircle": 15.25
    },
    {
        "NominalPipeSize": 12,
        "NumberofBolts": 16,
        "BoltSizeDiameter": "1-1/8",
        "BoltSize": 1.125,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "1-13/16",
        "MinYieldStress": 105000,
        "FlangeID": 12.88,
        "FlangeOD": 20.50,
        "BoltCircle": 17.75
    },
    {
        "NominalPipeSize": 14,
        "NumberofBolts": 20,
        "BoltSizeDiameter": "1-1/8",
        "BoltSize": 1.125,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "1-13/16",
        "MinYieldStress": 105000,
        "FlangeID": 14.19,
        "FlangeOD": 23.00,
        "BoltCircle": 20.25
    },
    {
        "NominalPipeSize": 16,
        "NumberofBolts": 20,
        "BoltSizeDiameter": "1-1/4",
        "BoltSize": 1.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2",
        "MinYieldStress": 105000,
        "FlangeID": 16.19,
        "FlangeOD": 25.50,
        "BoltCircle": 22.50
    },
    {
        "NominalPipeSize": 18,
        "NumberofBolts": 24,
        "BoltSizeDiameter": "1-1/4",
        "BoltSize": 1.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2",
        "MinYieldStress": 105000,
        "FlangeID": 18.19,
        "FlangeOD": 28.00,
        "BoltCircle": 24.75
    },
    {
        "NominalPipeSize": 20,
        "NumberofBolts": 24,
        "BoltSizeDiameter": "1-1/4",
        "BoltSize": 1.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2",
        "MinYieldStress": 105000,
        "FlangeID": 20.19,
        "FlangeOD": 30.50,
        "BoltCircle": 27.00
    },
    {
        "NominalPipeSize": 22,
        "NumberofBolts": 24,
        "BoltSizeDiameter": "1-1/4",
        "BoltSize": 1.250,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2",
        "MinYieldStress": 105000,
        "FlangeID": 22.19,
        "FlangeOD": 33.00,
        "BoltCircle": 29.25
    },
    {
        "NominalPipeSize": 24,
        "NumberofBolts": 24,
        "BoltSizeDiameter": "1-1/2",
        "BoltSize": 1.500,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/8",
        "MinYieldStress": 105000,
        "FlangeID": 24.19,
        "FlangeOD": 36.00,
        "BoltCircle": 32.00
    },
    {
        "NominalPipeSize": 26,
        "NumberofBolts": 28,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 28.00,
        "FlangeOD": 38.25,
        "BoltCircle": 34.50
    },
    {
        "NominalPipeSize": 28,
        "NumberofBolts": 28,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 30.00,
        "FlangeOD": 40.75,
        "BoltCircle": 37.00
    },
    {
        "NominalPipeSize": 30,
        "NumberofBolts": 28,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 32.00,
        "FlangeOD": 43.00,
        "BoltCircle": 39.25
    },
    {
        "NominalPipeSize": 32,
        "NumberofBolts": 28,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 34.00,
        "FlangeOD": 45.25,
        "BoltCircle": 41.50
    },
    {
        "NominalPipeSize": 34,
        "NumberofBolts": 28,
        "BoltSizeDiameter": "1-3/4",
        "BoltSize": 1.750,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "2-3/4",
        "MinYieldStress": 105000,
        "FlangeID": 36.00,
        "FlangeOD": 47.50,
        "BoltCircle": 43.50
    },
    {
        "NominalPipeSize": 36,
        "NumberofBolts": 32,
        "BoltSizeDiameter": "2",
        "BoltSize": 2.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/8",
        "MinYieldStress": 105000,
        "FlangeID": 38.00,
        "FlangeOD": 50.00,
        "BoltCircle": 46.00
    },
    {
        "NominalPipeSize": 38,
        "NumberofBolts": 32,
        "BoltSizeDiameter": "2",
        "BoltSize": 2.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/8",
        "MinYieldStress": 105000,
        "FlangeID": 40.00,
        "FlangeOD": 52.25,
        "BoltCircle": 48.00
    },
    {
        "NominalPipeSize": 40,
        "NumberofBolts": 36,
        "BoltSizeDiameter": "2",
        "BoltSize": 2.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/8",
        "MinYieldStress": 105000,
        "FlangeID": 42.00,
        "FlangeOD": 54.25,
        "BoltCircle": 50.25
    },
    {
        "NominalPipeSize": 42,
        "NumberofBolts": 36,
        "BoltSizeDiameter": "2",
        "BoltSize": 2.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/8",
        "MinYieldStress": 105000,
        "FlangeID": 44.00,
        "FlangeOD": 57.00,
        "BoltCircle": 52.75
    },
    {
        "NominalPipeSize": 44,
        "NumberofBolts": 36,
        "BoltSizeDiameter": "2",
        "BoltSize": 2.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/8",
        "MinYieldStress": 105000,
        "FlangeID": 46.00,
        "FlangeOD": 59.25,
        "BoltCircle": 55.00
    },
    {
        "NominalPipeSize": 46,
        "NumberofBolts": 40,
        "BoltSizeDiameter": "2",
        "BoltSize": 2.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/8",
        "MinYieldStress": 105000,
        "FlangeID": 48.00,
        "FlangeOD": 61.50,
        "BoltCircle": 57.25
    },
    {
        "NominalPipeSize": 48,
        "NumberofBolts": 40,
        "BoltSizeDiameter": "2",
        "BoltSize": 2.000,
        "ThreadsperInch": 8,
        "HeavyHexNutSize": "3-1/8",
        "MinYieldStress": 105000,
        "FlangeID": 50.00,
        "FlangeOD": 65.00,
        "BoltCircle": 60.75
    }
];
//# sourceMappingURL=ClassF_Values.js.map
var activeProductIds = [
    6673,
    7166,
    19006,
    65277096,
    65277103,
    63106282,
    35009101,
    62179609
];
//var activeProducts = [
//    {
//        title: "Corrosion Control for Buried Water Mains",
//        image: "6673_Def_L.png",
//        url: domain + storeProductBase + 6673,
//        listPrice: "$17.00",
//        memberPrice: "$11.00"
//    }, {
//        title: "Pipe Profile Series: Steel Pipe DVD",
//        image: "7166_Def_L.png",
//        url: domain + storeProductBase + 7166,
//        listPrice: "$130.00",
//        memberPrice: "$80.00"
//    }, {
//        title: "Steel Pipe Set Manual & Standards",
//        image: "19006_Def_L.png",
//        url: domain + storeProductBase + 19006,
//        listPrice: "$2,121.00",
//        memberPrice: "$1,257.00"
//    }, {
//        title: "Test",
//        image: "65277096_Def_L.png",
//        url: domain + storeProductBase + 65277096,
//        listPrice: "$95.00",
//        memberPrice: "$72.00"
//    }, {
//        title: "Test",
//        image: "65277103_Def_L.png",
//        url: domain + storeProductBase + 65277103,
//        listPrice: "$95.00",
//        memberPrice: "$72.00"
//    }, {
//        title: "Test",
//        image: "63106282_Def_L.png",
//        url: domain + storeProductBase + 63106282,
//        listPrice: "$95.00",
//        memberPrice: "$72.00"
//    }, {
//        title: "AWWA C207-13 Steel Pipe Flanges for Waterworks Service—Sizes 4 In. Through 144 In. (100 mm Through 3,600 mm)",
//        image: "35009101_Def_L.png",
//        url: domain + storeProductBase + 35009101,
//        listPrice: "$101.00",
//        memberPrice: "$62.00"
//    }, {
//        title: "Test",
//        image: "62179609_Def_L.png",
//        url: domain + storeProductBase + 62179609,
//        listPrice: "$95.00",
//        memberPrice: "$72.00"
//    }
//]; 
//# sourceMappingURL=RelatedProducts.js.map